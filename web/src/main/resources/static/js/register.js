var register = new Register();
function Register() {
    var that = this;

    this.initAgree = function() {
        var $widget = $('#agreement');
        var $button = $widget.find('button');
        var color = $button.data('color');
        var $checkbox = $widget.find('input:checkbox');
        var $footer = $("#t_and_c_m").find(".modal-footer");
        var settings = {
            on: {
                icon: 'glyphicon glyphicon-check'
            },
            off: {
                icon: 'glyphicon glyphicon-unchecked'
            }
        };
        $button.click(function () {
            $checkbox.prop('checked', !$checkbox.is(':checked'));
            $checkbox.triggerHandler('change');
            updateDisplay();
        });
        $checkbox.change(updateDisplay);
        $footer.find("button.agree").click(agree);
        $footer.find("button.disagree").click(disagree);

        updateDisplay();
        if ($button.find('.state-icon').length === 0) {
            $button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i>');
        }

        function agree() {
            $checkbox.prop('checked', true).change();
            updateDisplay();
        }

        function disagree() {
            $checkbox.prop('checked', false).change();
            updateDisplay();
        }

        function updateDisplay() {
            var isChecked = $checkbox.is(':checked');
            $button.data('state', (isChecked) ? "on" : "off");
            $button.find('.state-icon')
                .removeClass()
                .addClass('state-icon ' + settings[$button.data('state')].icon);
            if (isChecked) {
                $button
                    .removeClass('btn-default')
                    .addClass('btn-' + color + ' active');
            } else {
                $button
                    .removeClass('btn-' + color + ' active')
                    .addClass('btn-default');
            }
        }
    };

    this.initValidation = function() {
        var $email = $('#email');
        var $password = $('#password');
        var $registerButton = $('#register');
        var $passwordConfirmation = $('#password_confirmation');
        var $agreement = $('#agreement').find('input:checkbox');

        $(".validation-filed").change(validate);

        function validate() {
            var isValid = true;
            if($password.val() !== $passwordConfirmation.val() && ($password.val().length !== 0 || $passwordConfirmation.val().length !== 0)) {
                isValid = false;
                $("#wrongPasswordConfirmation").removeClass("hidden");
            } else {
                $("#wrongPasswordConfirmation").addClass("hidden");
            }
            if($email.val().length > 0) {
                $.ajax({
                    type: "POST",
                    url: "/verifyEmail",
                    data: {
                        email: $email.val()
                    },
                    success: function (resp) {
                        if(resp === 'error') {
                            isValid = false;
                            $("#alreadyExistEmail").removeClass("hidden");
                        } else {
                            $("#alreadyExistEmail").addClass("hidden");
                        }
                        $registerButton.prop('disabled', !$agreement.is(':checked') || !isValid);
                    }
                });
            } else {
                $registerButton.prop('disabled', !$agreement.is(':checked') || !isValid);
            }
        }
    };

    this.initRegister = function() {
        $('#register').click(function() {
            $.ajax({
                type: "POST",
                url: "/register",
                data: $("#registrationForm").serialize(),
                success: function (resp) {
                    if(resp === 'ok') {
                        document.location = '/login';
                    } else {
                        alert("Error. Please, try again.")
                    }
                }
            })
        })
    };

    this.init = function() {
        that.initAgree();
        that.initValidation();
        that.initRegister();
    }
}
$(document).ready(register.init);