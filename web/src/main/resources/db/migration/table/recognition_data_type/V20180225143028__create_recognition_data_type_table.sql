CREATE TABLE IF NOT EXISTS recognition_data_type (
  code VARCHAR(32) NOT NULL,
  name VARCHAR(128) NOT NULL,
  description VARCHAR(255) NOT NULL,
  PRIMARY KEY (code)
);

CREATE UNIQUE INDEX IF NOT EXISTS recognition_data_type_code_ux ON recognition_data_type(code ASC)