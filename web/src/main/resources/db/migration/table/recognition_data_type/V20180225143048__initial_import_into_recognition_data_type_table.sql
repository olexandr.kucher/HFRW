INSERT INTO recognition_data_type(code, name, description)
  SELECT 'HMF', 'Human Face', 'Human Face Recognition images'
  WHERE NOT EXISTS (SELECT 1 FROM recognition_data_type WHERE code = 'HMF');