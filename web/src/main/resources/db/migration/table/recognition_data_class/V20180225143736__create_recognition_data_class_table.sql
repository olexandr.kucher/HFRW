CREATE TABLE IF NOT EXISTS recognition_data_class (
  code VARCHAR(32) NOT NULL,
  name VARCHAR(128) NOT NULL,
  description VARCHAR(255) NOT NULL,

  type_code VARCHAR(32) NOT NULL,
  for_recognition BIT NOT NULL,
  PRIMARY KEY (code)
);

CREATE UNIQUE INDEX IF NOT EXISTS recognition_data_class_code_ux ON recognition_data_class(code);
CREATE INDEX IF NOT EXISTS recognition_data_class_type_code_idx ON recognition_data_class(type_code);