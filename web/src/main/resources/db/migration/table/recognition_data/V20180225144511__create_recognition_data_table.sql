CREATE TABLE IF NOT EXISTS recognition_data (
  id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  class_code VARCHAR(32) NOT NULL,
  image_format VARCHAR(32) NOT NULL,
  image_type VARCHAR(32) NOT NULL,
  image_width INT NOT NULL,
  image_height INT NOT NULL,
  image_size INT NOT NULL,
  image_content TEXT NOT NULL,
  parent_image_id INT NULL
);

CREATE UNIQUE INDEX IF NOT EXISTS recognition_data_id_ux ON recognition_data(id);
CREATE INDEX IF NOT EXISTS recognition_data_class_idx ON recognition_data(class_code);
CREATE INDEX IF NOT EXISTS recognition_data_parent_idx ON recognition_data(parent_image_id);