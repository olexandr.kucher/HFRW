CREATE TABLE IF NOT EXISTS user (
  username VARCHAR(64) NOT NULL,
  first_name VARCHAR(128) NULL,
  last_name VARCHAR(128) NULL,
  email VARCHAR(128) NOT NULL,
  password VARCHAR(128) NOT NULL,
  role VARCHAR(16) NOT NULL,
  PRIMARY KEY (username)
);

CREATE UNIQUE INDEX IF NOT EXISTS user_email_ux ON user (email);
CREATE UNIQUE INDEX IF NOT EXISTS user_username_ux ON user (username)