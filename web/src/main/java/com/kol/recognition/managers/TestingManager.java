package com.kol.recognition.managers;

import Jama.Matrix;
import com.google.common.collect.ComparisonChain;
import com.kol.metric.MetricType;
import com.kol.recognition.RecognitionAlgorithm;
import com.kol.recognition.common.Algorithm;
import com.kol.recognition.image.ImageConverter;
import com.kol.recognition.image.ImageRecognitionData;
import com.kol.recognition.image.ImageRepository;
import com.kol.recognition.processor.RecognitionProcessor;
import com.kol.recognition.settings.ClassifySettings;
import com.kol.recognition.settings.ClassifySettingsBuilder;
import com.kol.recognition.settings.ComponentProperty;
import com.kol.recognition.settings.RecognizerTrainType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import scala.Tuple2;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class TestingManager {
    private static final Logger logger = LoggerFactory.getLogger(TestingManager.class);

    private static final int MIN_IMAGES_FOR_STUDY = 1;
    private static final int MAX_IMAGES_FOR_STUDY = 10;
    private static final int IMAGE_FOR_CLASSIFICATION = 9;
    @Value("${hfr.knn.count}") public int knnCount;
    @Value("${face.image.width}") private int imageWidth;
    @Value("${face.image.height}") private int imageHeight;
    @Value("${hfr.recognition.images.type}") public String type;
    @Value("${hfr.principal.components.count}") public int pcaCount;
    @Value("${hfr.training.images.count}") public int trainingImages;

    @Value("${hfr.threshold.pca}") private double pcThreshold;
    @Value("${hfr.threshold.nbc}") private double nbcThreshold;
    @Value("${hfr.threshold.ica}") private double icaThreshold;
    @Value("${hfr.threshold.hash}") private double hashThreshold;

    private final RecognitionProcessor hfr;
    private final ImageRepository imageRepository;
    private final ImageConverter imageConverter;

    @Autowired
    public TestingManager(RecognitionProcessor hfr, ImageRepository imageRepository, ImageConverter imageConverter) {
        this.hfr = hfr;
        this.imageRepository = imageRepository;
        this.imageConverter = imageConverter;
    }

    public List<DisplayResult> classifyTests() {
        final Collection<String> classes = imageRepository.getClasses(type);
        final Map<String, ImageRecognitionData> mapping = new HashMap<>();
        for (String clazz : classes) {
            final List<ImageRecognitionData> images = imageRepository.findAllByClassCodeAndWidthAndHeight(clazz, imageWidth, imageHeight);
            mapping.put(clazz, getLastClassifyImage(images));
        }
        final List<Result> results = new ArrayList<>();
        for (ClassifySettings settings : generateAllSettings()) {
            logger.info("Start testing: " + settings.getAlgorithm().name() + " - " + settings.getNumberOfImages());
            for (Map.Entry<String, ImageRecognitionData> entry : mapping.entrySet()) {
                try {
                    final ClassifyResult classifyResult = classify(entry.getValue(), settings, entry.getKey());
                    results.add(new Result(entry.getKey(), settings.getNumberOfImages(), classifyResult, settings.getAlgorithm(), entry.getValue()));
                } catch (Throwable ignored) {
                    results.add(new Result(entry.getKey(), settings.getNumberOfImages(), ClassifyResult.ERROR, settings.getAlgorithm(), entry.getValue()));
                }
            }
            logger.info("Testing finished: " + settings.getAlgorithm().name() + " - " + settings.getNumberOfImages());
        }
        return createDisplayResults(results);
    }

    private List<DisplayResult> createDisplayResults(final List<Result> results) {
        final List<DisplayResult> displayResults = new ArrayList<>();
        final Map<RecognitionAlgorithm, List<Result>> algorithmGroup = results.stream().collect(Collectors.groupingBy(Result::getAlgorithm));
        for (Map.Entry<RecognitionAlgorithm, List<Result>> entry : algorithmGroup.entrySet()) {
            final Map<Integer, List<Result>> studyImgCountGroup = entry.getValue().stream().collect(Collectors.groupingBy(Result::getStudyImageCount));
            for (Map.Entry<Integer, List<Result>> ciEntry : studyImgCountGroup.entrySet()) {
                displayResults.add(new DisplayResult(
                        entry.getKey(),
                        ciEntry.getKey(),
                        countWrong(ciEntry.getValue()),
                        countCorrect(ciEntry.getValue()),
                        countErrors(ciEntry.getValue())
                ));
            }
        }
        return displayResults;
    }

    private int countWrong(final List<Result> results) {
        return count(results, ClassifyResult.WRONG);
    }

    private int countErrors(final List<Result> results) {
        return count(results, ClassifyResult.ERROR);
    }

    private int countCorrect(final List<Result> results) {
        return count(results, ClassifyResult.CORRECT);
    }

    private int count(final List<Result> results, final ClassifyResult classifyResult) {
        return (int) results.stream().filter(r -> r.classifyResult == classifyResult).count();
    }

    private ImageRecognitionData getLastClassifyImage(final List<ImageRecognitionData> images) {
        return images.get(Math.min(images.size() - 1, IMAGE_FOR_CLASSIFICATION));
    }

    private ClassifyResult classify(final ImageRecognitionData toClassify, final ClassifySettings settings, final String originClass) {
        final Algorithm algorithm = hfr.getAlgorithm(settings);
        final Tuple2<Boolean, String> classified = hfr.classifyFace(algorithm, imageConverter.createImageFromByteArray(toClassify.getByteContent()));
        if (classified._1()) {
            if (originClass.equalsIgnoreCase(classified._2())) {
                return ClassifyResult.CORRECT;
            } else {
                return ClassifyResult.WRONG;
            }
        } else {
            if (!originClass.equalsIgnoreCase(classified._2())) {
                return ClassifyResult.CORRECT;
            } else {
                return ClassifyResult.WRONG;
            }
        }
    }

    private List<ClassifySettings> generateAllSettings() {
        final List<ClassifySettings> settingsList = new ArrayList<>();
        for (RecognitionAlgorithm algorithm : RecognitionAlgorithm.values()) {
            for (int imageCount = MIN_IMAGES_FOR_STUDY; imageCount <= MAX_IMAGES_FOR_STUDY; imageCount++) {
                settingsList.add(generateSettings(algorithm, imageCount));
            }
        }
        return settingsList;
    }

    private ClassifySettings generateSettings(final RecognitionAlgorithm algorithm, final int imageCount) {
        return ClassifySettingsBuilder
                .start(knnCount, pcaCount, imageCount, type)
                .knn(knnCount, ComponentProperty.DEFAULT)
                .pca(pcaCount, ComponentProperty.DEFAULT)
                .metric(MetricType.EUCLIDEAN.get(Matrix.class))
                .images(imageCount)
                .algorithm(algorithm)
                .type(type)
                .distance(MetricType.HAMMING)
                .recognizerTrainType(RecognizerTrainType.FIRST)
                .threshold(pcThreshold, nbcThreshold, hashThreshold, icaThreshold)
                .result();
    }

    public enum ClassifyResult {
        CORRECT, WRONG, ERROR
    }

    public class Result {
        private String className;
        private int studyImageCount;
        private ClassifyResult classifyResult;
        private RecognitionAlgorithm algorithm;
        private ImageRecognitionData recognizedImage;

        public Result(String className, int studyImageCount, ClassifyResult classifyResult, RecognitionAlgorithm algorithm, ImageRecognitionData recognizedImage) {
            this.className = className;
            this.studyImageCount = studyImageCount;
            this.classifyResult = classifyResult;
            this.algorithm = algorithm;
            this.recognizedImage = recognizedImage;
        }

        public RecognitionAlgorithm getAlgorithm() {
            return algorithm;
        }

        public int getStudyImageCount() {
            return studyImageCount;
        }

        @Override
        public String toString() {
            return className + " (" + studyImageCount + ") -> " + algorithm + " (" + recognizedImage.getId() + "): " + classifyResult;
        }
    }

    public class DisplayResult implements Comparable<DisplayResult> {
        public static final String headerData = "Algorithm,ImageCount,CorrectCount,WrongCount,ErrorCount,TestsCount";
        private final RecognitionAlgorithm algorithm;
        private final int studyImageCount;
        private final int wrong;
        private final int correct;
        private final int error;

        public DisplayResult(RecognitionAlgorithm algorithm, int studyImageCount, int wrong, int correct, int error) {
            this.algorithm = algorithm;
            this.studyImageCount = studyImageCount;
            this.wrong = wrong;
            this.correct = correct;
            this.error = error;
        }

        @Override
        public String toString() {
            return algorithm + "," + studyImageCount + "," + correct + "," + wrong + "," + error + "," + (wrong + correct + error);
        }

        @Override
        public int compareTo(DisplayResult o) {
            return ComparisonChain
                    .start()
                    .compare(algorithm.getType(), o.algorithm.getType())
                    .compare(algorithm.name(), o.algorithm.name())
                    .compare(studyImageCount, o.studyImageCount)
                    .result();
        }
    }
}
