package com.kol.recognition.user;

import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface UserRepository extends CrudRepository<User, String> {
    @Query("SELECT u.* FROM User u WHERE u.email = :email")
    Optional<User> findByEmail(@Param("email") String email);
}
