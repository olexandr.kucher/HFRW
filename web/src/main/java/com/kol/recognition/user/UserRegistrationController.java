package com.kol.recognition.user;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("register")
public class UserRegistrationController {
    private final UserService userService;

    public UserRegistrationController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(value = "load")
    public String register() {
        return "register";
    }

    @PostMapping(value = "verifyEmail")
    @ResponseBody
    public ResponseEntity<?> verifyEmail(@RequestParam String email) {
        if (userService.isEmailUsed(email)) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        } else {
            return new ResponseEntity<>(HttpStatus.OK);
        }
    }

    @PostMapping(value = "register")
    @ResponseBody
    public ResponseEntity<?> register(@RequestBody User user) {
        if (userService.isEmailUsed(user.getEmail())) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
        userService.registerUser(user);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}
