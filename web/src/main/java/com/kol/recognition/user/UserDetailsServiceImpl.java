package com.kol.recognition.user;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Optional;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    private final UserService userService;

    public UserDetailsServiceImpl(UserService userService) {
        this.userService = userService;
    }

    @Override
    public UserDetails loadUserByUsername(String username) {
        final Optional<User> user = userService.findByEmail(username);
        if (user.isPresent()) {
            final User info = user.get();
            return new org.springframework.security.core.userdetails.User(
                    info.getEmail(), info.getPassword(),
                    Collections.singletonList(new SimpleGrantedAuthority(info.getRole()))
            );
        } else {
            throw new UsernameNotFoundException("User has not been found.");
        }
    }
}
