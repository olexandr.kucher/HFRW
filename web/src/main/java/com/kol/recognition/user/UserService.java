package com.kol.recognition.user;

import java.util.Optional;

public interface UserService {
    Optional<User> findByEmail(String email);

    boolean isEmailUsed(String email);

    User registerUser(User user);
}
