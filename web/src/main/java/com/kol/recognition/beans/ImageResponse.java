package com.kol.recognition.beans;

public class ImageResponse {
    private Integer fileId;
    private String status;
    private Integer width;
    private Integer height;
    private String src;

    public Integer getFileId() {
        return fileId;
    }

    public ImageResponse setFileId(Integer fileId) {
        this.fileId = fileId;
        return this;
    }

    public String getStatus() {
        return status;
    }

    public ImageResponse setStatus(String status) {
        this.status = status;
        return this;
    }

    public Integer getWidth() {
        return width;
    }

    public ImageResponse setWidth(Integer width) {
        this.width = width;
        return this;
    }

    public Integer getHeight() {
        return height;
    }

    public ImageResponse setHeight(Integer height) {
        this.height = height;
        return this;
    }

    public String getSrc() {
        return src;
    }

    public ImageResponse setSrc(String src) {
        this.src = src;
        return this;
    }
}
