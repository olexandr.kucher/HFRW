package com.kol.recognition.config;

import com.kol.recognition.image.ImageRepository;
import com.kol.recognition.processor.CachedHFRProcessor;
import com.kol.recognition.processor.HFRProcessor;
import com.kol.recognition.processor.RecognitionProcessor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;

@Configuration
public class RecognitionConfig {
    @Bean
    @Primary
    @ConditionalOnProperty(value = "hfr.use.cache", havingValue = "true")
    public RecognitionProcessor cacheRecognitionProcessor(ImageRepository imageRepository, Environment environment) {
        return new CachedHFRProcessor(
                imageRepository,
                environment.getRequiredProperty("face.image.width", Integer.class),
                environment.getRequiredProperty("face.image.height", Integer.class),
                environment.getRequiredProperty("hfr.hash.width", Integer.class),
                environment.getRequiredProperty("hfr.hash.height", Integer.class)
        );
    }

    @Bean
    @ConditionalOnMissingBean
    public RecognitionProcessor recognitionProcessor(ImageRepository imageRepository, Environment environment) {
        return new HFRProcessor(
                imageRepository,
                environment.getRequiredProperty("face.image.width", Integer.class),
                environment.getRequiredProperty("face.image.height", Integer.class),
                environment.getRequiredProperty("hfr.hash.width", Integer.class),
                environment.getRequiredProperty("hfr.hash.height", Integer.class)
        );
    }
}
