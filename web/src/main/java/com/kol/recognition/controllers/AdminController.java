package com.kol.recognition.controllers;

import com.google.common.collect.Lists;
import com.kol.recognition.image.ImageConverterImpl;
import com.kol.recognition.image.ImageRecognitionData;
import com.kol.recognition.image.ImageService;
import com.kol.recognition.managers.TestingManager;
import org.imgscalr.Scalr;
import org.springframework.core.env.Environment;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.io.Files.getFileExtension;
import static org.springframework.util.SystemPropertyUtils.resolvePlaceholders;

@RestController
@RequestMapping(value = "admin")
public class AdminController {
    private final int imageWidth;
    private final int imageHeight;
    private final ImageService imageService;
    private final TestingManager testingManager;
    private final ImageConverterImpl imageManager;

    public AdminController(ImageService imageService, TestingManager testingManager, ImageConverterImpl imageManager, Environment environment) {
        this.imageService = imageService;
        this.testingManager = testingManager;
        this.imageManager = imageManager;
        this.imageWidth = environment.getRequiredProperty("face.image.width", Integer.class);
        this.imageHeight = environment.getRequiredProperty("face.image.height", Integer.class);
    }

    @GetMapping("importTrainingFaces")
    public String importTrainingFaces() throws Exception {
        final Path training = Paths.get(ResourceUtils.getURL(resolvePlaceholders("classpath:training")).toURI());
        final List<Path> recognitionTypes = Files.list(training).collect(Collectors.toList());
        for (Path recognitionType : recognitionTypes) {
            final String type = recognitionType.getFileName().toString();
            imageService.deleteAllByClassCodeIn(imageService.getClasses(type));
            final List<Path> subjects = Files.list(recognitionType).collect(Collectors.toList());
            for (Path subject : subjects) {
                final List<Path> images = Files.list(subject).collect(Collectors.toList());
                for (Path imagePath : images) {
                    final BufferedImage src = ImageIO.read(Files.newInputStream(imagePath));
                    final BufferedImage image;
                    if (src.getWidth() != imageWidth || src.getHeight() != imageHeight) {
                        image = Scalr.resize(src, Scalr.Mode.FIT_EXACT, imageWidth, imageHeight);
                    } else {
                        image = src;
                    }
                    final ImageRecognitionData imageRecognitionData = imageManager.createRecognitionDataImage(image, getFileExtension(imagePath.getFileName().toString()));
                    imageRecognitionData.setClassCode(type + subject.getFileName().toString());
                    imageRecognitionData.setType(type);
                    imageService.save(imageRecognitionData);
                }
            }
        }
        return "ok";
    }

    @GetMapping("classifyTests")
    public String classifyTests() throws Exception {
        final Path resultFile = Paths.get("e:/projects/HFRW/test_results.csv");
        final List<TestingManager.DisplayResult> results = testingManager.classifyTests();
        if (!Files.exists(resultFile)) {
            Files.createFile(resultFile);
        }
        final ArrayList<String> list = Lists.newArrayList(TestingManager.DisplayResult.headerData);
        list.addAll(results
                .stream()
                .sorted()
                .map(TestingManager.DisplayResult::toString)
                .collect(Collectors.toList())
        );
        Files.write(resultFile, list);
        return "ok";
    }
}
