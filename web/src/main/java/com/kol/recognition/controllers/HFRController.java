package com.kol.recognition.controllers;

import com.kol.perceptualhash.hash.PerceptualHash;
import com.kol.recognition.RecognitionAlgorithm;
import com.kol.recognition.beans.HFRForm;
import com.kol.recognition.common.Algorithm;
import com.kol.recognition.common.ImageUtils;
import com.kol.recognition.image.ImageConverter;
import com.kol.recognition.image.ImageConverterImpl;
import com.kol.recognition.image.ImageRecognitionData;
import com.kol.recognition.image.ImageService;
import com.kol.recognition.processor.RecognitionProcessor;
import com.kol.recognition.settings.ClassifySettings;
import com.kol.recognition.settings.ClassifySettingsBuilder;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import scala.Tuple2;

import java.awt.image.BufferedImage;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "hfr")
public class HFRController {
    private static final String PRINCIPAL_COMPONENT = "principal_component";

    @Value("${hfr.knn.count}") private int knnCount;
    @Value("${face.image.width}") private int imageWidth;
    @Value("${face.image.height}") private int imageHeight;
    @Value("${hfr.recognition.images.type}") private String type;
    @Value("${hfr.principal.components.count}") private int pcaCount;
    @Value("${hfr.training.images.count}") private int trainingImages;

    @Value("${hfr.threshold.pca}") private double pcThreshold;
    @Value("${hfr.threshold.nbc}") private double nbcThreshold;
    @Value("${hfr.threshold.ica}") private double icaThreshold;
    @Value("${hfr.threshold.hash}") private double hashThreshold;

    private final RecognitionProcessor hfr;
    private final ImageService imageService;
    private final ImageConverter imageManager;

    public HFRController(RecognitionProcessor hfr, ImageService imageService, ImageConverter imageManager) {
        this.hfr = hfr;
        this.imageService = imageService;
        this.imageManager = imageManager;
    }

    @RequestMapping(value = "classify")
    public String classify(@ModelAttribute HFRForm form) {
        final Optional<ImageRecognitionData> imageRecognitionData = imageService.findById(form.getFileId());
        final JSONObject answer = new JSONObject();
        if (imageRecognitionData.isPresent()) {
            final Algorithm algorithm = hfr.getAlgorithm(getSettings(form));
            final Tuple2<Boolean, String> classified = hfr.classifyFace(
                    algorithm, imageManager.createImageFromByteArray(imageRecognitionData.get().getByteContent())
            );
            if (classified._1()) {
                answer.put("status", "ok");
                answer.put("class", classified._2());
                answer.put("storedImages", getTrainingImages(classified._2(), algorithm));
            } else {
                answer.put("status", "error");
                answer.put("reason", "Distance is out of threshold");
            }
        } else {
            answer.put("status", "error");
            answer.put("reason", "NoSuchFileException");
        }
        return answer.toString();
    }

    @RequestMapping(value = "eigenVectors")
    public String eigenVectors(@ModelAttribute HFRForm form) {
        final Optional<ImageRecognitionData> imageRecognitionData = imageService.findById(form.getFileId());
        final JSONObject answer = new JSONObject();
        if (imageRecognitionData.isPresent()) {
            final ClassifySettings settings = getSettings(form);
            final Algorithm algorithm = hfr.getAlgorithm(settings);
            final Collection<ImageRecognitionData> names = savePrincipalComponentImages(algorithm, settings.getAlgorithm());
            answer.put("status", "ok");
            answer.put("storedImages", getEigenVectorImages(names));
        } else {
            answer.put("status", "error");
            answer.put("reason", "NoSuchFileException");
        }
        return answer.toString();
    }

    @RequestMapping(value = "hashImage")
    public String hashImage(@ModelAttribute HFRForm form) {
        final Optional<ImageRecognitionData> imageRecognitionData = imageService.findById(form.getFileId());
        final JSONObject answer = new JSONObject();
        if (imageRecognitionData.isPresent()) {
            final ClassifySettings settings = getSettings(form);
            final PerceptualHash hash = hfr.hash(settings.getAlgorithm());
            final List<BufferedImage> hashImages = hash.getHashImage(ImageUtils.fromByteArray(imageRecognitionData.get().getByteContent()));
            final Collection<ImageRecognitionData> imageRecognitionDataList = hashImages.stream()
                    .map(image -> imageManager.createRecognitionDataImage(image, ImageConverterImpl.DEFAULT_IMAGE_FORMAT))
                    .peek(image -> image.setClassCode(settings.getAlgorithm().name()))
                    .collect(Collectors.toList());
            imageService.save(imageRecognitionData.get());
            answer.put("status", "ok");
            answer.put("storedImages", getEigenVectorImages(imageRecognitionDataList));
        } else {
            answer.put("status", "error");
            answer.put("reason", "NoSuchFileException");
        }
        return answer.toString();
    }

    private JSONArray getTrainingImages(String className, Algorithm algorithm) {
        return new JSONArray(algorithm.getTraining().get(className).stream().map(c -> "/picture/loadImage/" + c.getId()).collect(Collectors.toList()));
    }

    private JSONArray getEigenVectorImages(Collection<ImageRecognitionData> images) {
        return new JSONArray(images.stream().map(im -> "/picture/loadImage/" + im.getId()).collect(Collectors.toList()));
    }

    private Collection<ImageRecognitionData> savePrincipalComponentImages(Algorithm classifier, RecognitionAlgorithm algorithm) {
        final List<BufferedImage> images = classifier.getProcessedImages();
        final Collection<ImageRecognitionData> imageRecognitionData = images.stream()
                .map(image -> imageManager.createRecognitionDataImage(image, ImageConverterImpl.DEFAULT_IMAGE_FORMAT))
                .peek(image -> image.setClassCode(algorithm.name()))
                .peek(image -> image.setType(PRINCIPAL_COMPONENT))
                .collect(Collectors.toList());
        return imageService.saveAll(imageRecognitionData);
    }

    private ClassifySettings getSettings(HFRForm form) {
        return ClassifySettingsBuilder
                .start(knnCount, pcaCount, trainingImages, type)
                .knn(form.getKnnValue(), form.getKnnType())
                .pca(form.getComponentsValue(), form.getComponentsType())
                .metric(form.getMetricType())
                .images(form.getTrainingValue())
                .algorithm(form.getAlgorithm())
                .type(form.getRecognitionType())
                .distance(form.getStringDistanceType())
                .recognizerTrainType(form.getTrainingType())
                .threshold(pcThreshold, nbcThreshold, hashThreshold, icaThreshold)
                .result();
    }
}