package com.kol.recognition.controllers;

import com.google.common.base.Strings;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping
public class MainController {
    private final int imageWidth;
    private final int imageHeight;
    private final String recognitionType;

    public MainController(Environment environment) {
        this.imageWidth = environment.getRequiredProperty("face.image.width", Integer.class);
        this.imageHeight = environment.getRequiredProperty("face.image.height", Integer.class);
        this.recognitionType = environment.getRequiredProperty("hfr.recognition.images.type", String.class);
    }

    @GetMapping("/")
    public String start(ModelMap map) {
        map.put("width", imageWidth);
        map.put("height", imageHeight);
        map.put("recognitionType", recognitionType);
        return "index";
    }

    @GetMapping("login")
    public String login(@RequestParam(value = "error", required = false) String error, ModelMap map) {
        map.put("wrongCredentials", !Strings.isNullOrEmpty(error));
        return "login";
    }
}
