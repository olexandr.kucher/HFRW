package com.kol.recognition.image;

import com.kol.recognition.common.Image;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import javax.persistence.Entity;
import java.util.Base64;

@Entity
@Table("recognition_data")
public class ImageRecognitionData {
    private Integer id;
    private String classCode;
    private String type;
    private Integer size;
    private String format;
    private Integer width;
    private Integer height;
    private String content;
    private Integer parentImage;

    public ImageRecognitionData() {}

    public ImageRecognitionData(Integer id, String classCode) {
        this.id = id;
        this.classCode = classCode;
    }

    @Id
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column("class_code")
    public String getClassCode() {
        return classCode;
    }

    public void setClassCode(String classCode) {
        this.classCode = classCode;
    }

    @Column("image_type")
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Column("image_size")
    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    @Column("image_format")
    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    @Column("image_width")
    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    @Column("image_height")
    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    @Column("image_content")
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public byte[] getByteContent() {
        return Base64.getDecoder().decode(content);
    }

    public void setContent(byte[] content) {
        this.content = Base64.getEncoder().encodeToString(content);
    }

    @Column("parent_image_id")
    public Integer getParentImage() {
        return parentImage;
    }

    public void setParentImage(Integer parentImage) {
        this.parentImage = parentImage;
    }

    public Image toImage() {
        final Image im = new Image();
        im.setClazz(classCode);
        im.setContent(getByteContent());
        im.setFormat(format);
        im.setHeight(height);
        im.setSize(size);
        im.setWidth(width);
        im.setId(getId());
        return im;
    }
}
