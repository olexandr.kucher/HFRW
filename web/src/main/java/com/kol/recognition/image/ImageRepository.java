package com.kol.recognition.image;

import com.kol.recognition.ProcessorDAO;
import com.kol.recognition.beans.StoredImageResponse;
import com.kol.recognition.common.Image;
import org.springframework.data.jdbc.repository.query.Modifying;
import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public interface ImageRepository extends CrudRepository<ImageRecognitionData, Integer>, ProcessorDAO {
    @Query("SELECT r.* FROM recognition_data r WHERE image_type = :type")
    List<ImageRecognitionData> findDistinctImagesByType(@Param("type") String type);

    @Query("SELECT id, image_type AS type, class_code "
            + "FROM recognition_data WHERE image_width = :width AND image_height = :height "
            + "AND image_type = :type AND class_code NOT IN (:codes)"
    )
    List<StoredImageResponse> findAllByWidthAndHeightAndTypeAndClassCodeNotIn(@Param("width") int width, @Param("height") int height,
                                                                              @Param("type") String type, @Param("codes") Collection<String> codes);

    @Query("SELECT * FROM recognition_data WHERE image_width = :width AND image_height = :height AND class_code = :code")
    List<ImageRecognitionData> findAllByClassCodeAndWidthAndHeight(@Param("code") String classCode, @Param("width") int width, @Param("height") int height);

    @Modifying
    @Query("DELETE FROM recognition_data WHERE class_code IN (:codes)")
    void deleteAllByClassCodeIn(@Param("codes") Collection<String> codes);

    @Override
    default Collection<String> getClasses(String type) {
        return findDistinctImagesByType(type)
                .stream()
                .map(ImageRecognitionData::getClassCode)
                .filter(cl -> !EXCLUDED_IMAGE_CLASSES.contains(cl))
                .distinct().collect(Collectors.toList());
    }

    @Override
    default Collection<Image> getImages(String classCode, int width, int height) {
        return findAllByClassCodeAndWidthAndHeight(classCode, width, height).stream()
                .map(ImageRecognitionData::toImage)
                .collect(Collectors.toList());
    }
}
