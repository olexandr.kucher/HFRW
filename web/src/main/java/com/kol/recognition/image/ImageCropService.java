package com.kol.recognition.image;

import com.kol.recognition.beans.CropRequest;
import com.kol.recognition.beans.ImageUploadRequest;
import com.kol.recognition.beans.StoredImageResponse;

import java.util.List;

public interface ImageCropService {
    ImageRecognitionData cropAndSave(ImageRecognitionData imageRecognitionData, CropRequest cropRequest);

    ImageRecognitionData uploadAndSave(ImageUploadRequest request);

    List<StoredImageResponse> findAllStoredImagesByType(String type);
}
