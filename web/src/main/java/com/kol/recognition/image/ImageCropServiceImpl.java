package com.kol.recognition.image;

import com.kol.recognition.ProcessorDAO;
import com.kol.recognition.beans.CropRequest;
import com.kol.recognition.beans.ImageUploadRequest;
import com.kol.recognition.beans.StoredImageResponse;
import org.imgscalr.Scalr;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import javax.annotation.PreDestroy;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.List;

@Service
public class ImageCropServiceImpl implements ImageCropService {
    private static final String IMAGE_CLASS_CROPPED_CODE = "CRPD";
    private static final String IMAGE_CLASS_UPLOADED_CODE = "UPLD";

    private final int imageWidth;
    private final int imageHeight;
    private final String recognitionType;
    private final ImageService imageService;
    private final ImageConverter imageConverter;

    public ImageCropServiceImpl(ImageService imageService, ImageConverter imageConverter,
                                Environment environment) {
        this.imageService = imageService;
        this.imageConverter = imageConverter;
        imageWidth = environment.getRequiredProperty("face.image.width", Integer.class);
        imageHeight = environment.getRequiredProperty("face.image.height", Integer.class);
        recognitionType = environment.getRequiredProperty("hfr.recognition.images.type", String.class);
    }

    @Override
    public ImageRecognitionData cropAndSave(ImageRecognitionData image, CropRequest cropRequest) {
        final BufferedImage templateImage = resize(
                crop(image.getByteContent(), cropRequest),
                imageWidth, imageHeight
        );

        final ImageRecognitionData imageRecognitionData = imageConverter.createRecognitionDataImage(templateImage, image.getFormat());
        imageRecognitionData.setClassCode(IMAGE_CLASS_CROPPED_CODE);
        imageRecognitionData.setParentImage(image.getId());
        imageRecognitionData.setType(recognitionType);
        return imageService.save(imageRecognitionData);
    }

    @Override
    public ImageRecognitionData uploadAndSave(ImageUploadRequest request) {
        final byte[] binaryData;
        if(ImageConverterImpl.BASE64_IMAGE_TYPE.equals(request.getType())){
            binaryData = imageConverter.extractImageFromBase64(request.getSrc());
        } else {
            binaryData = imageConverter.extractImageFromUrl(request.getSrc());
        }

        final ImageRecognitionData imageRecognitionData = imageConverter.createRecognitionDataImage(binaryData, ImageConverterImpl.DEFAULT_IMAGE_FORMAT);
        imageRecognitionData.setClassCode(IMAGE_CLASS_UPLOADED_CODE);
        imageRecognitionData.setType(recognitionType);
        return imageService.save(imageRecognitionData);
    }

    @Override
    public List<StoredImageResponse> findAllStoredImagesByType(String type) {
        return imageService.findAllByWidthAndHeightAndType(imageWidth, imageHeight, type);
    }

    @PreDestroy
    private void close() {
        imageService.deleteAllByClassCodeIn(ProcessorDAO.EXCLUDED_IMAGE_CLASSES);
    }

    private BufferedImage crop(byte[] binaryData, CropRequest cropRequest) {
        final BufferedImage src = imageConverter.createImageFromByteArray(binaryData);
        final BufferedImage dst = new BufferedImage(cropRequest.getWidth(), cropRequest.getHeight(), BufferedImage.TYPE_BYTE_GRAY);
        final Graphics2D g = dst.createGraphics();
        g.drawImage(src, 0, 0, cropRequest.getWidth(), cropRequest.getHeight(), cropRequest.getX1(),
                cropRequest.getY1(), cropRequest.getX1() + cropRequest.getWidth(), cropRequest.getY1() + cropRequest.getHeight(), null);
        g.dispose();
        return dst;
    }

    private BufferedImage resize(BufferedImage originalImage, int width, int height) {
        return Scalr.resize(originalImage, Scalr.Method.ULTRA_QUALITY, width, height);
    }
}
