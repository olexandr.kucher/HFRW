package com.kol.recognition.image;

import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import com.kol.recognition.ProcessorDAO;
import com.kol.recognition.beans.CropRequest;
import com.kol.recognition.beans.ImageResponse;
import com.kol.recognition.beans.ImageUploadRequest;
import com.kol.recognition.beans.StoredImageResponse;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("picture")
public class ImageController {
    private final ImageService imageService;
    private final ImageCropService imageCropService;

    public ImageController(ImageService imageService, ImageCropService imageCropService) {
        this.imageService = imageService;
        this.imageCropService = imageCropService;
    }

    @GetMapping("loadImage/{fileId}")
    public void loadImage(@PathVariable Integer fileId, HttpServletResponse response) throws IOException {
        final Optional<ImageRecognitionData> image = imageService.findById(fileId);
        if(image.isPresent()) {
            response.setContentType("image/" + image.get().getFormat());
            response.setContentLength(image.get().getSize());
            try (OutputStream os = response.getOutputStream()) {
                os.write(image.get().getByteContent());
            }
        }
    }

    @PostMapping("crop/{fileId}")
    public ResponseEntity<ImageResponse> crop(@PathVariable Integer fileId, @RequestBody CropRequest cropRequest) {
        final Optional<ImageRecognitionData> image = imageService.findById(fileId);
        if(image.isPresent()) {
            final ImageRecognitionData data = imageCropService.cropAndSave(image.get(), cropRequest);
            return new ResponseEntity<>(new ImageResponse()
                    .setFileId(data.getId())
                    .setWidth(data.getWidth())
                    .setHeight(data.getHeight())
                    .setSrc("/picture/loadImage/" + data.getId()), HttpStatus.OK
            );
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("upload")
    public ResponseEntity<ImageResponse> upload(@RequestBody ImageUploadRequest request) {
        final ImageRecognitionData data = imageCropService.uploadAndSave(request);
        return new ResponseEntity<>(new ImageResponse()
                .setFileId(data.getId())
                .setWidth(data.getWidth())
                .setHeight(data.getHeight())
                .setSrc("/picture/loadImage/" + data.getId()), HttpStatus.OK
        );
    }

    @GetMapping("storedImages")
    public String loadAllStoredImages(@RequestParam String type) {
        final List<StoredImageResponse> imageRecognitionData = imageCropService.findAllStoredImagesByType(type);
        final JSONObject images = new JSONObject();
        final Multimap<String, StoredImageResponse> map = Multimaps.index(imageRecognitionData, StoredImageResponse::getClassCode);
        for (String key : map.keySet()) {
            images.put(key, map.get(key).stream().map(image -> "/picture/loadImage/" + image.getId()).collect(Collectors.toList()));
        }
        return images.toString();
    }

    @PostMapping("deleteProcessed")
    public void deleteProcessed() {
        imageService.deleteAllByClassCodeIn(ProcessorDAO.EXCLUDED_IMAGE_CLASSES);
    }
}