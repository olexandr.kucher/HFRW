package com.kol.recognition.image;

import com.kol.recognition.beans.StoredImageResponse;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface ImageService {
    Optional<ImageRecognitionData> findById(Integer id);

    List<StoredImageResponse> findAllByWidthAndHeightAndType(int imageWidth, int imageHeight, String type);

    ImageRecognitionData save(ImageRecognitionData image);

    void deleteAllByClassCodeIn(Set<String> imageClasses);

    Set<String> getClasses(String type);

    List<ImageRecognitionData> saveAll(Collection<ImageRecognitionData> imageRecognitionData);
}
