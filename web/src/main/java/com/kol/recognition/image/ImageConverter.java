package com.kol.recognition.image;

import java.awt.image.BufferedImage;

public interface ImageConverter {
    byte[] extractImageFromBase64(String src);

    byte[] extractImageFromUrl(String url);

    byte[] convertImageToByteArray(BufferedImage image, String format);

    ImageRecognitionData createRecognitionDataImage(BufferedImage image, String format);

    ImageRecognitionData createRecognitionDataImage(byte[] binaryData, String format);

    BufferedImage createImageFromByteArray(byte[] binaryData);
}
