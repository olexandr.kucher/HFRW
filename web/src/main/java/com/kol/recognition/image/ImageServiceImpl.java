package com.kol.recognition.image;

import com.google.common.collect.Lists;
import com.kol.recognition.beans.StoredImageResponse;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static com.kol.recognition.ProcessorDAO.EXCLUDED_IMAGE_CLASSES;

@Service
public class ImageServiceImpl implements ImageService {
    private final ImageRepository imageRepository;

    public ImageServiceImpl(ImageRepository imageRepository) {
        this.imageRepository = imageRepository;
    }

    @Override
    public Optional<ImageRecognitionData> findById(Integer id) {
        return imageRepository.findById(id);
    }

    @Override
    public List<StoredImageResponse> findAllByWidthAndHeightAndType(int imageWidth, int imageHeight, String type) {
        return imageRepository.findAllByWidthAndHeightAndTypeAndClassCodeNotIn(imageWidth, imageHeight, type, EXCLUDED_IMAGE_CLASSES);
    }

    @Override
    public ImageRecognitionData save(ImageRecognitionData image) {
        return imageRepository.save(image);
    }

    @Override
    public void deleteAllByClassCodeIn(Set<String> imageClasses) {
        imageRepository.deleteAllByClassCodeIn(imageClasses);
    }

    @Override
    public Set<String> getClasses(String type) {
        return new HashSet<>(imageRepository.getClasses(type));
    }

    @Override
    public List<ImageRecognitionData> saveAll(Collection<ImageRecognitionData> imageRecognitionData) {
        return Lists.newArrayList(imageRepository.saveAll(imageRecognitionData));
    }
}
