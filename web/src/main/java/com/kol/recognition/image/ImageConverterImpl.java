package com.kol.recognition.image;

import org.springframework.stereotype.Component;
import org.springframework.util.FileCopyUtils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Base64;

@Component
public class ImageConverterImpl implements ImageConverter {
    public static final String DEFAULT_IMAGE_FORMAT = "bmp";
    public static final String BASE64_IMAGE_TYPE = "base64";

    @Override
    public byte[] extractImageFromBase64(String src) {
        return Base64.getDecoder().decode(src);
    }

    @Override
    public byte[] extractImageFromUrl(String url) {
        try(InputStream is = new URL(url).openConnection().getInputStream();
            ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            FileCopyUtils.copy(is, baos);
            return baos.toByteArray();
        } catch (IOException e) {
            throw new IllegalArgumentException("Image cannot be read from url.");
        }
    }

    @Override
    public byte[] convertImageToByteArray(BufferedImage image, String format) {
        try (ByteArrayOutputStream output = new ByteArrayOutputStream()) {
            ImageIO.write(image, format, output);
            return output.toByteArray();
        } catch (IOException ignored) {
            throw new IllegalArgumentException("Image cannot be converted to byte array.");
        }
    }

    @Override
    public ImageRecognitionData createRecognitionDataImage(BufferedImage image, String format) {
        final ImageRecognitionData imageRecognitionData = new ImageRecognitionData();
        final byte[] binaryData = convertImageToByteArray(image, format);
        imageRecognitionData.setFormat(format);
        imageRecognitionData.setWidth(image.getWidth());
        imageRecognitionData.setHeight(image.getHeight());
        imageRecognitionData.setSize(binaryData.length);
        imageRecognitionData.setContent(binaryData);
        return imageRecognitionData;
    }

    @Override
    public ImageRecognitionData createRecognitionDataImage(byte[] binaryData, String format) {
        return createRecognitionDataImage(createImageFromByteArray(binaryData), format);
    }

    @Override
    public BufferedImage createImageFromByteArray(byte[] binaryData) {
        try {
            return ImageIO.read(new ByteArrayInputStream(binaryData));
        } catch (IOException e) {
            throw new IllegalArgumentException("Image cannot be created from byte array.");
        }
    }
}
