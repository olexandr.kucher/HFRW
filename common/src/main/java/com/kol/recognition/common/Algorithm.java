package com.kol.recognition.common;

import com.google.common.collect.Multimap;
import scala.Tuple2;

import java.awt.image.BufferedImage;
import java.util.List;

public interface Algorithm {

    Tuple2<Boolean, String> classify(BufferedImage image);

    Multimap<String, Image> getTraining();

    List<BufferedImage> getProcessedImages();
}