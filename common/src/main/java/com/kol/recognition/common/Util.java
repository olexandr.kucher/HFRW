package com.kol.recognition.common;

public class Util {
    public static String leadingZeros(final String str, final int length) {
        if (str.length() >= length) {
            return str;
        } else {
            return String.format("%0" + (length - str.length()) + "d%s", 0, str);
        }
    }
}
