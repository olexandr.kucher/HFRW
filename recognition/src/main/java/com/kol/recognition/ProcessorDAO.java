package com.kol.recognition;

import com.google.common.collect.Sets;
import com.kol.recognition.common.Image;

import java.util.Arrays;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

public interface ProcessorDAO {
    Set<String> EXCLUDED_IMAGE_CLASSES = getExcludedImageClasses();

    static Set<String> getExcludedImageClasses() {
        final Set<String> excludedImageClasses = Sets.newHashSet();
        excludedImageClasses.add("CRPD");
        excludedImageClasses.add("UPLD");
        excludedImageClasses.addAll(
                Arrays.stream(RecognitionAlgorithm.values())
                        .map(Enum::name)
                        .collect(Collectors.toSet())
        );
        return excludedImageClasses;
    }

    Collection<String> getClasses(String type);

    Collection<Image> getImages(String classCode, int width, int height);
}
