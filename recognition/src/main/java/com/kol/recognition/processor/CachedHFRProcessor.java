package com.kol.recognition.processor;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.kol.recognition.common.Algorithm;
import com.kol.recognition.ProcessorDAO;
import com.kol.recognition.settings.ClassifySettings;

import java.util.concurrent.TimeUnit;

public class CachedHFRProcessor extends RecognitionProcessor {

    private final Cache<ClassifySettings, Algorithm> cache = CacheBuilder.newBuilder()
            .expireAfterAccess(10, TimeUnit.MINUTES).build();

    public CachedHFRProcessor(ProcessorDAO dao, int width, int height, int hashWidth, int hashHeight) {
        super(dao, width, height, hashWidth, hashHeight);
    }

    @Override
    public Algorithm getAlgorithm(final ClassifySettings settings) {
        final Algorithm ifPresent = cache.getIfPresent(settings);
        final Algorithm algorithm;
        if (null == ifPresent) {
            algorithm = algorithm(settings);
            cache.put(settings, algorithm);
        } else {
            algorithm = ifPresent;
        }
        return algorithm;
    }
}