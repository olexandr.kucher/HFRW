package com.kol.recognition.processor;

import com.kol.recognition.common.Algorithm;
import com.kol.recognition.ProcessorDAO;
import com.kol.recognition.settings.ClassifySettings;

public class HFRProcessor extends RecognitionProcessor {

    public HFRProcessor(ProcessorDAO dao, int width, int height, int hashWidth, int hashHeight) {
        super(dao, width, height, hashWidth, hashHeight);
    }

    @Override
    public Algorithm getAlgorithm(final ClassifySettings settings) {
        return algorithm(settings);
    }
}