package com.kol.recognition.processor;

import Jama.Matrix;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.kol.analysis.bean.AnalysisSettings;
import com.kol.metric.Metric;
import com.kol.perceptualhash.Hash;
import com.kol.perceptualhash.hash.AverageHash;
import com.kol.perceptualhash.hash.DCTHash;
import com.kol.perceptualhash.hash.PerceptualHash;
import com.kol.recognition.ProcessorDAO;
import com.kol.recognition.RecognitionAlgorithm;
import com.kol.recognition.common.Algorithm;
import com.kol.recognition.common.Image;
import com.kol.recognition.common.ImageUtils;
import com.kol.recognition.placeholder.AnalysisSettingsPlaceholder;
import com.kol.recognition.placeholder.HashSettingsPlaceholder;
import com.kol.recognition.placeholder.NBCSettingsPlaceholder;
import com.kol.recognition.settings.ClassifySettings;
import scala.Tuple2;

import java.awt.image.BufferedImage;
import java.util.Collection;
import java.util.stream.Collectors;

public abstract class RecognitionProcessor {

    private final int width;
    private final int height;
    private final int hashWidth;
    private final int hashHeight;
    private final ProcessorDAO dao;

    public RecognitionProcessor(ProcessorDAO dao, int width, int height, int hashWidth, int hashHeight) {
        this.dao = dao;
        this.width = width;
        this.height = height;
        this.hashWidth = hashWidth;
        this.hashHeight = hashHeight;
    }

    public Tuple2<Boolean, String> classifyFace(final Algorithm algorithm, final BufferedImage image) {
        return algorithm.classify(image);
    }

    public abstract Algorithm getAlgorithm(final ClassifySettings settings);

    private Algorithm trainComponentAlgorithm(final ClassifySettings settings) {
        final Collection<String> classes = dao.getClasses(settings.getType());

        /** read images from database ang choose some of them (or all) for train the recognizer **/
        final Multimap<String, Image> training = ArrayListMultimap.create();
        classes.forEach(classCode -> training.putAll(
                classCode, settings.getTrainType().getTrainObjectIds(
                        settings.getNumberOfImages(),
                        dao.getImages(classCode, width, height).stream().collect(Collectors.toList())
                )
        ));

        /** transform images, which were choose for training, into Jama.Matrix **/
        final Multimap<String, Matrix> data = ArrayListMultimap.create();
        for (String label : training.keySet()) {
            data.putAll(label, training.get(label).stream().map(ImageUtils::toVector).collect(Collectors.toList()));
        }
        /** get Recognizer based on exist data **/
        final AnalysisSettingsPlaceholder placeholder = new AnalysisSettingsPlaceholder();
        placeholder.setData(data);
        placeholder.setTrain(training);
        placeholder.setVecLength(width * height);
        placeholder.setComponents(settings.getComponents());

        final AnalysisSettings analysisSettings = new AnalysisSettings();
        analysisSettings.setKnnCount(settings.getKnnCount());
        analysisSettings.setMetric(settings.getMetric());
        analysisSettings.setWidth(width);
        analysisSettings.setHeight(height);
        analysisSettings.setThreshold(settings.getThreshold());
        placeholder.setSettings(analysisSettings);

        return settings.getAlgorithm().get(placeholder);
    }

    private Algorithm trainHashAlgorithm(final ClassifySettings settings) {
        final RecognitionAlgorithm algorithm = settings.getAlgorithm();

        final Collection<String> classes = dao.getClasses(settings.getType());
        final Multimap<String, Image> training = ArrayListMultimap.create();
        classes.forEach(classCode -> training.putAll(classCode,
                settings.getTrainType().getTrainObjectIds(
                        settings.getNumberOfImages(),
                        dao.getImages(classCode, width, height).stream().collect(Collectors.toList())
                )
        ));

        final PerceptualHash hash = hash(algorithm);
        final Multimap<String, Hash> data = ArrayListMultimap.create();
        for (String label : training.keySet()) {
            data.putAll(label, training.get(label).stream().map(i -> hash.getHash(i.getContent())).collect(Collectors.toList()));
        }

        final HashSettingsPlaceholder placeholder = new HashSettingsPlaceholder();
        final Metric<String> objectMetric = settings.getDistanceType().get(String.class);
        placeholder.setDistance(objectMetric);
        placeholder.setHeight(hashHeight);
        placeholder.setWidth(hashWidth);
        placeholder.setData(data);
        placeholder.setTrain(training);
        placeholder.setThreshold(settings.getThreshold());
        return algorithm.get(placeholder);
    }

    private Algorithm trainClassifyAlgorithm(final ClassifySettings settings) {
        final Collection<String> classes = dao.getClasses(settings.getType());

        /** read images from database ang choose some of them (or all) for train the recognizer **/
        final Multimap<String, Image> training = ArrayListMultimap.create();
        classes.forEach(classCode -> training.putAll(
                classCode, settings.getTrainType().getTrainObjectIds(
                        settings.getNumberOfImages(),
                        dao.getImages(classCode, width, height).stream().collect(Collectors.toList())
                )
        ));

        /** transform images, which were choose for training, into Jama.Matrix **/
        final Multimap<String, BufferedImage> data = ArrayListMultimap.create();
        for (String label : training.keySet()) {
            data.putAll(label, training.get(label).stream().map(v -> ImageUtils.fromByteArray(v.getContent())).collect(Collectors.toList()));
        }
        /** get Recognizer based on exist data **/
        final NBCSettingsPlaceholder placeholder = new NBCSettingsPlaceholder();
        placeholder.setData(data);
        placeholder.setTrain(training);
        placeholder.setWidth(width);
        placeholder.setHeight(height);
        placeholder.setThreshold(settings.getThreshold());

        return settings.getAlgorithm().get(placeholder);
    }

    public PerceptualHash hash(final RecognitionAlgorithm algorithm) {
        switch(algorithm) {
            case AHASH:
                return new AverageHash(hashWidth, hashHeight);
            case DCT_HASH:
                return new DCTHash(hashWidth, hashHeight);
            default:
                throw new RuntimeException("Incorrect Perceptual Hash Algorithm selected: " + algorithm);
        }
    }

    protected Algorithm algorithm(final ClassifySettings settings) {
        final RecognitionAlgorithm algorithm = settings.getAlgorithm();
        switch(algorithm.getType()) {
            case PCA_BASED:
            case ICA_BASED:
            case CLASSIFY_COMPONENT:
                return trainComponentAlgorithm(settings);
            case HASH:
                return trainHashAlgorithm(settings);
            case CLASSIFY:
                return trainClassifyAlgorithm(settings);
            default:
                throw new RuntimeException("Incorrect Recognition Algorithm Type: " + algorithm.getType());
        }
    }
}
