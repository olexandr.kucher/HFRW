package com.kol.recognition;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Main {
    private static final Path basePath = Paths.get("e:\\projects\\HFRW");
    private static final List<String> modules = Arrays.asList(
            "common", "component-analysis", "discrete-cosine-transform", "fast-ica",
            "metrics", "naive-bayes-classifier", "perceptual-hash", "recognition", "src"
    );
    private static final List<String> content = new ArrayList<>();
    private static int count = 0;

    public static void main(String[] args) throws IOException {
//        sourceExtract();
        batchRename("E:\\data\\downloads\\yalefaces");
    }

    private static void batchRename(final String basePath) throws IOException {
        final Path curDir = Paths.get(basePath);
        final List<Path> list = Files.list(curDir).collect(Collectors.toList());
        for (Path path : list) {
            if(!Files.isDirectory(path)) {
                final String fileName = path.getFileName().toString();
                final Path subjectDir = curDir.resolve("renamed").resolve(fileName.substring(0, fileName.indexOf(".")));
                if(!Files.exists(subjectDir)) {
                    Files.createDirectory(subjectDir);
                }
                Files.write(subjectDir.resolve(fileName + ".gif"), Files.readAllBytes(path));
            }
        }
    }

    private static void sourceExtract() throws IOException {
        for (String module : modules) {
            final Path resolve = basePath.resolve(module);
            content.add("/**" + resolve.getFileName() + "**/");
            process(resolve);
            content.add("============================================================================================");
            content.add("============================================================================================");
            content.add("============================================================================================");
        }
        Files.write(Paths.get("e:\\projects\\HFRW\\result.txt"), content);
        System.out.println(count);
    }

    private static void process(final Path path) throws IOException {
        if(Files.isDirectory(path)) {
            final List<Path> paths = Files.list(path).collect(Collectors.toList());
            for (Path pp : paths) {
                process(pp);
            }
        } else if(path.toString().endsWith(".java") || path.toString().endsWith(".scala")){
            content.add("/**" + path.getFileName() + "**/");
            final List<String> collect = Files.readAllLines(path)
                    .stream()
                    .filter(l -> !l.startsWith("import ") && !l.startsWith("package "))
                    .collect(Collectors.toList());
            content.addAll(collect.stream().filter(l -> !l.isEmpty()).collect(Collectors.toList()));
            count++;
        }
    }
}
