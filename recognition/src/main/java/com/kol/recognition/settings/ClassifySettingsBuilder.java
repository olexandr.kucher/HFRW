package com.kol.recognition.settings;

import Jama.Matrix;
import com.google.common.base.Strings;
import com.kol.metric.Metric;
import com.kol.metric.MetricType;
import com.kol.recognition.RecognitionAlgorithm;

public final class ClassifySettingsBuilder {

    private int knn;
    private int pca;
    private int images;
    private String type;
    private ClassifySettings settings;

    private ClassifySettingsBuilder(){
        this.settings = new ClassifySettings();
    }

    private ClassifySettingsBuilder(int knn, int pca, int images, String type) {
        this();
        this.knn = knn;
        this.pca = pca;
        this.images = images;
        this.type = type;
    }

    public static ClassifySettingsBuilder start(final int knn, final int pca, final int images, final String type) {
        return new ClassifySettingsBuilder(knn, pca, images, type);
    }

    public ClassifySettingsBuilder knn(final Integer value, final ComponentProperty cp) {
        if(ComponentProperty.DEFAULT.equals(cp) || null == value){
            settings.setKnnCount(knn);
        } else {
            settings.setKnnCount(value);
        }
        return this;
    }

    public ClassifySettingsBuilder metric(final MetricType value) {
        if(null != value) {
            settings.setMetric(value.get(Matrix.class));
        }
        return this;
    }

    public ClassifySettingsBuilder algorithm(final RecognitionAlgorithm algorithm) {
        settings.setAlgorithm(algorithm);
        return this;
    }

    public ClassifySettingsBuilder pca(final Integer value, final ComponentProperty cp) {
        if(ComponentProperty.DEFAULT.equals(cp) || null == value){
            settings.setComponents(pca);
        } else {
            settings.setComponents(value);
        }
        return this;
    }

    public ClassifySettingsBuilder images(final Integer value) {
        if(null == value){
            settings.setNumberOfImages(images);
        } else {
            settings.setNumberOfImages(value);
        }
        return this;
    }

    public ClassifySettings result() {
        return settings;
    }

    public static ClassifySettingsBuilder start() {
        return new ClassifySettingsBuilder();
    }

    public ClassifySettingsBuilder knn(final int value) {
        settings.setKnnCount(value);
        return this;
    }

    public ClassifySettingsBuilder metric(final Metric<Matrix> metric) {
        settings.setMetric(metric);
        return this;
    }

    public ClassifySettingsBuilder type(final String value) {
        if(Strings.isNullOrEmpty(value)){
            settings.setType(type);
        } else {
            settings.setType(value);
        }
        return this;
    }

    public ClassifySettingsBuilder distance(final MetricType distanceType) {
        settings.setDistanceType(distanceType);
        return this;
    }

    public ClassifySettingsBuilder recognizerTrainType(final RecognizerTrainType trainType) {
        settings.setTrainType(trainType);
        return this;
    }

    public ClassifySettingsBuilder threshold(final double pcThreshold, final double nbcThreshold, final double hashThreshold, final double icaThreshold) {
        final RecognitionAlgorithm algorithm = settings.getAlgorithm();
        switch(algorithm.getType()) {
            case PCA_BASED:
                settings.setThreshold(pcThreshold);
                break;
            case ICA_BASED:
                settings.setThreshold(icaThreshold);
                break;
            case HASH:
                settings.setThreshold(hashThreshold);
                break;
            case CLASSIFY:
            case CLASSIFY_COMPONENT:
                settings.setThreshold(nbcThreshold);
                break;
            default:
                settings.setThreshold(Double.NaN);
                break;
        }
        return this;
    }
}