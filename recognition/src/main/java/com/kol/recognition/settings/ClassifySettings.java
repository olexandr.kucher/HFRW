package com.kol.recognition.settings;

import Jama.Matrix;
import com.kol.metric.Metric;
import com.kol.metric.MetricType;
import com.kol.recognition.RecognitionAlgorithm;

public final class ClassifySettings {

    private Metric<Matrix> metric;
    private RecognitionAlgorithm algorithm;

    private int knnCount;
    private int components;
    private int numberOfImages;

    private String type;
    private RecognizerTrainType trainType;
    private MetricType distanceType;

    private double threshold;

    public Metric<Matrix> getMetric() {
        return metric;
    }

    public void setMetric(Metric<Matrix> metric) {
        this.metric = metric;
    }

    public RecognitionAlgorithm getAlgorithm() {
        return algorithm;
    }

    public void setAlgorithm(RecognitionAlgorithm algorithm) {
        this.algorithm = algorithm;
    }

    public int getKnnCount() {
        return knnCount;
    }

    public void setKnnCount(int knnCount) {
        this.knnCount = knnCount;
    }

    public int getComponents() {
        return components;
    }

    public void setComponents(int components) {
        this.components = components;
    }

    public int getNumberOfImages() {
        return numberOfImages;
    }

    public void setNumberOfImages(int numberOfImages) {
        this.numberOfImages = numberOfImages;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public RecognizerTrainType getTrainType() {
        return trainType;
    }

    public void setTrainType(RecognizerTrainType trainType) {
        this.trainType = trainType;
    }

    public MetricType getDistanceType() {
        return distanceType;
    }

    public void setDistanceType(MetricType distanceType) {
        this.distanceType = distanceType;
    }

    public double getThreshold() {
        return threshold;
    }

    public void setThreshold(double threshold) {
        this.threshold = threshold;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ClassifySettings that = (ClassifySettings) o;

        if (knnCount != that.knnCount) return false;
        if (components != that.components) return false;
        if (numberOfImages != that.numberOfImages) return false;
        if (Double.compare(that.threshold, threshold) != 0) return false;
        if (metric != null ? !metric.getClass().equals(that.metric.getClass()) : that.metric != null) return false;
        if (algorithm != that.algorithm) return false;
        if (type != null ? !type.equals(that.type) : that.type != null) return false;
        if (trainType != that.trainType) return false;
        return distanceType == that.distanceType;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = metric != null ? metric.getClass().getSimpleName().hashCode() : 0;
        result = 31 * result + (algorithm != null ? algorithm.hashCode() : 0);
        result = 31 * result + knnCount;
        result = 31 * result + components;
        result = 31 * result + numberOfImages;
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (trainType != null ? trainType.hashCode() : 0);
        result = 31 * result + (distanceType != null ? distanceType.hashCode() : 0);
        temp = Double.doubleToLongBits(threshold);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
