package com.kol.recognition;

import com.kol.analysis.recognizer.ica.ICA;
import com.kol.analysis.recognizer.pcabased.LDA;
import com.kol.analysis.recognizer.pcabased.LPP;
import com.kol.analysis.recognizer.pcabased.PCA;
import com.kol.perceptualhash.hash.AverageHash;
import com.kol.perceptualhash.hash.DCTHash;
import com.kol.perceptualhash.hash.HashRecognizer;
import com.kol.recognition.common.Algorithm;
import com.kol.recognition.nbc.NBCR;
import com.kol.recognition.placeholder.AnalysisSettingsPlaceholder;
import com.kol.recognition.placeholder.HashSettingsPlaceholder;
import com.kol.recognition.placeholder.SettingsPlaceholder;

public enum RecognitionAlgorithm {
    PCA(AlgorithmType.PCA_BASED) {
        @Override
        public <T extends SettingsPlaceholder> Algorithm get(final T placeholder) {
            final AnalysisSettingsPlaceholder settings = (AnalysisSettingsPlaceholder) placeholder;
            return new PCA(settings.getData(), settings.getComponents(), settings.getVecLength(), settings.getTrain(), settings.getSettings());
        }
    },
    LDA(AlgorithmType.PCA_BASED) {
        @Override
        public <T extends SettingsPlaceholder> Algorithm get(final T placeholder) {
            final AnalysisSettingsPlaceholder settings = (AnalysisSettingsPlaceholder) placeholder;
            return new LDA(settings.getData(), settings.getComponents(), settings.getVecLength(), settings.getTrain(), settings.getSettings());
        }
    },
    NBC(AlgorithmType.CLASSIFY_COMPONENT) {
        @Override
        public <T extends SettingsPlaceholder> Algorithm get(final T placeholder) {
            final AnalysisSettingsPlaceholder settings = (AnalysisSettingsPlaceholder) placeholder;
            return new NBCR(settings.getData(), settings.getTrain(), ((AnalysisSettingsPlaceholder) placeholder).getSettings().getThreshold());
        }
    },
    /*NBC_C(AlgorithmType.CLASSIFY) {
        @Override
        public <T extends SettingsPlaceholder> Algorithm get(final T placeholder) {
            final NBCSettingsPlaceholder settings = (NBCSettingsPlaceholder) placeholder;
            return new NBCRecognizer(settings.getData(), settings.getTrain(), settings.getWidth(), settings.getHeight(), settings.getThreshold());
        }
    },*/
    LPP(AlgorithmType.PCA_BASED) {
        @Override
        public <T extends SettingsPlaceholder> Algorithm get(final T placeholder) {
            final AnalysisSettingsPlaceholder settings = (AnalysisSettingsPlaceholder) placeholder;
            return new LPP(settings.getData(), settings.getComponents(), settings.getVecLength(), settings.getTrain(), settings.getSettings());
        }
    },
    DCT_HASH(AlgorithmType.HASH) {
        @Override
        public <T extends SettingsPlaceholder> Algorithm get(T placeholder) {
            final HashSettingsPlaceholder settings = (HashSettingsPlaceholder) placeholder;
            return new HashRecognizer(new DCTHash(settings.getWidth(), settings.getHeight()), settings.getDistance(), settings.getData(), settings.getTrain(), settings.getThreshold());
        }
    },
    AHASH(AlgorithmType.HASH) {
        @Override
        public <T extends SettingsPlaceholder> Algorithm get(T placeholder) {
            final HashSettingsPlaceholder settings = (HashSettingsPlaceholder) placeholder;
            return new HashRecognizer(new AverageHash(settings.getWidth(), settings.getHeight()), settings.getDistance(), settings.getData(), settings.getTrain(), settings.getThreshold());
        }
    },
    FAST_ICA(AlgorithmType.ICA_BASED) {
        @Override
        public <T extends SettingsPlaceholder> Algorithm get(final T placeholder) {
            final AnalysisSettingsPlaceholder settings = (AnalysisSettingsPlaceholder) placeholder;
            return new ICA(settings.getData(), settings.getComponents(), settings.getVecLength(), settings.getTrain(), settings.getSettings());
        }
    };

    RecognitionAlgorithm(AlgorithmType type) {
        this.type = type;
    }

    private AlgorithmType type;

    public AlgorithmType getType() {
        return type;
    }

    public void setType(AlgorithmType type) {
        this.type = type;
    }

    public abstract <T extends SettingsPlaceholder> Algorithm get(final T placeholder);

    public enum AlgorithmType {
        PCA_BASED, HASH, CLASSIFY, ICA_BASED, CLASSIFY_COMPONENT
    }
}