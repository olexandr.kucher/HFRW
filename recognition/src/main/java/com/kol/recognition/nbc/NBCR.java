package com.kol.recognition.nbc;

import Jama.Matrix;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import com.kol.nbc.NBLA;
import com.kol.nbc.NaiveBayesLearningAlgorithm;
import com.kol.recognition.common.Algorithm;
import com.kol.recognition.common.Image;
import com.kol.recognition.common.ImageUtils;
import scala.Tuple2;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

public class NBCR implements Algorithm {
    private final NaiveBayesLearningAlgorithm<double[]> nbc;
    private final Multimap<String, Image> train;
    private final Matrix meanMatrix;
    private final double threshold;

    public NBCR(Multimap<String, Matrix> data, Multimap<String, Image> train, double threshold) {
        this.train = train;
        this.threshold = threshold;
        this.nbc = NBLA.doubleArray(8);
        final Multimap<String, Matrix> matrixData = Multimaps.transformValues(data, ImageUtils::toVector);
        this.meanMatrix = countMean(new ArrayList<>(matrixData.values()));
        matrixData.entries().forEach(e -> nbc.addExample(e.getValue().minus(meanMatrix).getArray()[0], e.getKey()));
    }

    /**
     * The matrix has already been vectorized
     */
    private Matrix countMean(final List<Matrix> list) {
        final Matrix all = new Matrix(list.get(0).getRowDimension(), 1);
        list.forEach(all::plusEquals);
        return all.times(1.0 / list.size());
    }

    @Override
    public Tuple2<Boolean, String> classify(BufferedImage image) {
        final Tuple2<String, Object> classified = nbc.classifier().classify(ImageUtils.toVector(image).minus(meanMatrix).getArray()[0]);
        return (Double) classified._2() >= threshold ? new Tuple2<>(true, classified._1()) : new Tuple2<>(false, classified._1());
    }

    @Override
    public Multimap<String, Image> getTraining() {
        return train;
    }

    @Override
    public List<BufferedImage> getProcessedImages() {
        return Lists.newArrayList();
    }
}
