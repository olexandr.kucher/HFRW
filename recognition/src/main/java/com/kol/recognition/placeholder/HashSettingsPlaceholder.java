package com.kol.recognition.placeholder;

import com.google.common.collect.Multimap;
import com.kol.metric.Metric;
import com.kol.perceptualhash.Hash;
import com.kol.recognition.common.Image;

public class HashSettingsPlaceholder implements SettingsPlaceholder {

    private int width;
    private int height;
    private Metric<String> distance;

    private Multimap<String, Hash> data;
    private Multimap<String, Image> train;

    private double threshold;

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public Metric<String> getDistance() {
        return distance;
    }

    public void setDistance(Metric<String> distance) {
        this.distance = distance;
    }

    public Multimap<String, Hash> getData() {
        return data;
    }

    public void setData(Multimap<String, Hash> data) {
        this.data = data;
    }

    public Multimap<String, Image> getTrain() {
        return train;
    }

    public void setTrain(Multimap<String, Image> train) {
        this.train = train;
    }

    public double getThreshold() {
        return threshold;
    }

    public void setThreshold(double threshold) {
        this.threshold = threshold;
    }
}
