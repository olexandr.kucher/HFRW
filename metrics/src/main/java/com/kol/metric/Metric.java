package com.kol.metric;

public interface Metric<T> {
    double getDistance(T first, T second);
}