package com.kol.metric.string;

import com.kol.metric.Metric;
import org.apache.commons.lang3.StringUtils;

public final class JaroWinklerDistance implements Metric<String> {
    @Override
    public double getDistance(final String first, final String second) {
        return StringUtils.getJaroWinklerDistance(first, second);
    }
}
