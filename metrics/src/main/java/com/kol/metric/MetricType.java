package com.kol.metric;

import Jama.Matrix;
import com.kol.metric.matrix.CosineDissimilarity;
import com.kol.metric.matrix.EuclideanDistance;
import com.kol.metric.matrix.L1Distance;
import com.kol.metric.string.HammingDistance;
import com.kol.metric.string.JaroWinklerDistance;
import com.kol.metric.string.LevensteinDistance;

public enum MetricType {
    HAMMING {
        public Metric<String> get() {
            return new HammingDistance();
        }
    },
    JARO_WINKLER {
        public Metric<String> get() {
            return new JaroWinklerDistance();
        }
    },
    LEVENSTEIN {
        public Metric<String> get() {
            return new LevensteinDistance();
        }
    },
    COSINE {
        public Metric<Matrix> get() {
            return new CosineDissimilarity();
        }
    },
    L1D {
        public Metric<Matrix> get() {
            return new L1Distance();
        }
    },
    EUCLIDEAN {
        public Metric<Matrix> get() {
            return new EuclideanDistance();
        }
    };

    public abstract Metric get();
    
    public <T> Metric<T> get(Class<T> clazz) {
        return get();
    }
}