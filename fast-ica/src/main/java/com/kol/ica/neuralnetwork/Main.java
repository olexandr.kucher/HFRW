package com.kol.ica.neuralnetwork;

import java.util.Random;

public class Main {
    public static void main(String args[]){
        int K = 5000;
        double [][] s = new double[3][K];
        double dt = 0.001;
        Random rand = new Random();
        for(int k=0; k< K; k++){
            s[0][k] = rand.nextGaussian();
            s[1][k] =
                    Math.pow(10, 0)*Math.sin(500*dt*k)*Math.sin(30*dt*k);
            s[2][k] = Math.pow(10, 0)*Math.sin(234*dt*k);

        }
        new MultiPlot("s",s);
        double[][] A = {{0.20, -0.61, 0.62},
                {0.91, -0.89, -0.33},
                {0.59,  0.76, 0.60}};
        double [][] x = new double[3][K];
        for(int k=0; k<K; k++){
            for(int i=0; i<3; i++){
                for(int j=0; j<3; j++){
                    x[i][k] = x[i][k] + A[i][j]*s[j][k];
                }
            }

        }
        new MultiPlot("x",x);
        NeuralICA ica = new NeuralICA(3);
        final Jama.Matrix ica1 = ica.ica(new Jama.Matrix(x), K);
        new MultiPlot("y",ica1.getArray());
    }
}
