package com.kol.ica.neuralnetwork;

import Jama.Matrix;

import java.util.function.Function;

/**
 * Performs independent component analysis using neural network
 * Matrix W is defined by learning rule. There are a lot of learning rules performed in real-time (see sources).
 */
public class NeuralICA {
    private Matrix W;
    private Matrix identity;
    private final int icCount;
    private final static double eta = 0.01;
    private final Function<Double, Double> f;
    private final Function<Double, Double> g;

    public NeuralICA(int icCount, Function<Double, Double> f, Function<Double, Double> g) {
        this.icCount = icCount;
        identity = Matrix.identity(icCount, icCount);
        W = Matrix.identity(icCount, icCount);
        this.f = f;
        this.g = g;
    }

    /**
     * computes function f(y) = y
     * computes function g(y) = tanh(10*y)
     * For statistically independent signals the covariance matrix of odd functions f(y) and g(y) is diagonal
     * @param icCount - number of independent components
     */
    public NeuralICA(int icCount) {
        this(icCount, Function.<Double>identity(), y -> Math.tanh(10 * y));
    }

    public Matrix ica(final Matrix x, final int iter) {
        final Matrix y = new Matrix(icCount, iter);
        for (int k = 0; k < iter; k++) {
            setCol(y, ica(getCol(x, k)), k);
        }
        return y;
    }

    /**
     * receives vector x of observations for a certain time moment and returns update of output vector of separated signals for this time.
     * It performs two operations:
     *      y = W*x
     *      W = W * n[I-f(y)*g^T(y)]*W
     */
    private double[] ica(final double[] x) {
        final Matrix xm = new Matrix(x.length, 1);
        for (int i = 0; i < x.length; i++) {
            xm.set(i, 0, x[i]);
        }
        final Matrix y = W.times(xm);
        W.plusEquals(dW(apply(y, f), apply(y, g)).times(W));
        return y.getColumnPackedCopy();
    }

    /**
     *
     */
    private Matrix apply(final Matrix y, final Function<Double, Double> func) {
        final Matrix matrix = y.copy();
        for (int i = 0; i < matrix.getRowDimension(); i++) {
            for (int j = 0; j < matrix.getColumnDimension(); j++) {
                matrix.set(i, j, func.apply(matrix.get(i, j)));
            }
        }
        return matrix;
    }

    /**
     * receives functions f and g and performs dW = eta*(identity - f(y)*g(y)) and returns dW
     */
    private Matrix dW(final Matrix f, final Matrix g) {
        final Matrix matrix = new Matrix(f.getRowDimension(), f.getRowDimension());
        for (int i = 0; i < f.getRowDimension(); i++) {
            for (int j = 0; j < f.getRowDimension(); j++) {
                matrix.set(i, j, eta * (identity.get(i, j) - f.get(i, 0) * g.get(j, 0)));
            }
        }
        return matrix;
    }

    private double[] getCol(final Matrix matrix, final int col) {
        final double[] y = new double[matrix.getRowDimension()];
        for (int i = 0; i < matrix.getRowDimension(); i++) {
            y[i] = matrix.get(i, col);
        }
        return y;
    }

    private void setCol(final Matrix matrix, final double[] b, final int col) {
        for (int i = 0; i < matrix.getRowDimension(); i++) {
            matrix.set(i, col, b[i]);
        }
    }
}