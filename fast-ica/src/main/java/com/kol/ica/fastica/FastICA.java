package com.kol.ica.fastica;

import org.ejml.simple.SimpleEVD;
import org.ejml.simple.SimpleMatrix;
import org.ejml.simple.SimpleSVD;

import java.util.Arrays;
import java.util.Collections;
import java.util.Random;

/**
 * FastICA port of Python scikit-learn implementation.
 */
public class FastICA {

    /**
     * The matrix that projects the data into the ICA domain (the estimated unMixing matrix)
     * unMixingMatrix double[][] matrix containing estimated independent component projection matrix
     */
    private SimpleMatrix unMixingMatrix;

    /**
     * The pre-whitening matrix that was used on the data (defaults to the identity matrix; the pre-whitening matrix)
     * preWhitening double[][] matrix containing the pre-whitening matrix
     */
    private SimpleMatrix preWhitening;

    // the product of preWhitening and unMixingMatrix
    private SimpleMatrix KW;

    // The data matrix
    private SimpleMatrix dataMatrix;

    // The mean value of each column of the input matrix
    private SimpleMatrix meanMatrix;

    // Number of components to output
    private int numberOfComponents;

    // Reference to non-linear neg-entropy estimator function
    private final NegativeEntropyEstimator entropyEstimator;

    // Convergence tolerance
    private final double tolerance;

    // Iteration limit
    private final int iterationLimit;

    // Whiten the data if true
    private final boolean whiten;

    /**
     * Default FastICA instance using the LogCosh() function to estimate
     * negative entropy and whitening/mean-centering of the data matrix with
     * simple default values.
     */
    public FastICA() {
        this(new LogCosh(), 1E-5, 200, true);
    }

    /**
     * General FastICA instance constructor with an arbitrary (user-supplied)
     * function to estimate negative entropy. This implementation does not
     * perform automatic component selection or reduction.
     *
     * @param entropyEstimator - {@link NegativeEntropyEstimator} to estimate negative entropy
     * @param tolerance - maximum allowable convergence error
     * @param iterationLimit - max number of iterations
     * @param whiten - whiten the data matrix (default true)
     */
    public FastICA(NegativeEntropyEstimator entropyEstimator, double tolerance, int iterationLimit, boolean whiten) {
        this.entropyEstimator = entropyEstimator;
        this.tolerance = tolerance;
        this.iterationLimit = iterationLimit;
        this.whiten = whiten;
    }

    /**
     * Estimate the unmixing matrix for the data provided
     *
     * @param data - 2d array of doubles containing the data; each column
     *             contains a single signal, and each row contains one sample of all
     *             signals (rows contain instances, columns are features)
     */
    public void fit(final double[][] data, int num_components) {
        dataMatrix = new SimpleMatrix(data);
        // mean center the attributes in dataMatrix
        meanMatrix = new SimpleMatrix(new double[][]{center(dataMatrix)});

        // get the size parameter of the symmetric unMixingMatrix matrix; size cannot be larger than the number of samples or the number of features
        numberOfComponents = Collections.min(Arrays.asList(dataMatrix.numRows(), dataMatrix.numCols(), num_components));

        preWhitening = SimpleMatrix.identity(this.numberOfComponents);  // init preWhitening
        if (whiten) {
            dataMatrix = whiten(dataMatrix);  // sets preWhitening
        }

        // start with an orthogonal initial unMixingMatrix matrix drawn from a standard Normal distribution
        unMixingMatrix = symmetricDeCorrelation(gaussianSquareMatrix(num_components));

        // fit the data
        parallelICA();  // solves for unMixingMatrix

        // Store the resulting transformation matrix
        KW = preWhitening.mult(unMixingMatrix);
    }

    /**
     * Project a row-indexed matrix of data into the ICA domain by applying
     * the pre-whitening and un-mixing matrices. This method should not be
     * called prior to running fit() with input data.
     *
     * @param data rectangular double[][] array containing values; the
     *             number of columns should match the data provided to the
     *             fit() method for training
     * @return result    rectangular double[][] array containing the projected output values
     */
    public double[][] transform(final double[][] data) {
        return mToA(new SimpleMatrix(data).minus(meanMatrix).mult(KW));
    }

    /**
     * FastICA main loop - using default symmetric decorrelation. (i.e., estimate all the independent components in parallel)
     */
    private void parallelICA() {
        final int n = dataMatrix.numCols();
        for (int iter = 0; iter < iterationLimit; iter++) {
            // Estimate the negative entropy and first derivative average
            entropyEstimator.estimate(dataMatrix.mult(unMixingMatrix));

            // Update the unMixingMatrix matrix
            SimpleMatrix nextW = dataMatrix.transpose().mult(entropyEstimator.getElementWise()).scale(1.0 / (double) n);
            for (int i = 0; i < numberOfComponents; i++) {
                final SimpleMatrix newRow = nextW.extractVector(true, i);
                final SimpleMatrix oldRow = unMixingMatrix.extractVector(true, i);
                nextW.insertIntoThis(i, 0, newRow.minus(oldRow.elementMult(entropyEstimator.getColumnWise())));
            }
            nextW = symmetricDeCorrelation(nextW);

            // Test convergence criteria for unMixingMatrix
            double lim = 0;
            for (int i = 0; i < unMixingMatrix.numRows(); i++) {
                final SimpleMatrix newRow = nextW.extractVector(true, i);
                final SimpleMatrix oldRow = unMixingMatrix.extractVector(true, i);
                double tmp = newRow.dot(oldRow.transpose());
                tmp = Math.abs(Math.abs(tmp) - 1);
                if (tmp > lim) {
                    lim = tmp;
                }
            }
            unMixingMatrix = nextW;

            if (lim < tolerance) {
                return;
            }
        }
        throw new RuntimeException("ICA did not converge - try again with more iterations.");
    }

    /**
     * Whiten a matrix of column vectors by decorrelating and scaling the
     * elements according to: x_new = ED^{-1/2}E'x , where E is the
     * orthogonal matrix of eigenvectors of E{xx'}. In this implementation
     * (based on the FastICA sklearn Python package) the eigen decomposition is
     * replaced with the SVD.
     *
     * The decomposition is ambiguous with regard to the direction of
     * column vectors (they can be either +/- without changing the result).
     */
    private SimpleMatrix whiten(final SimpleMatrix x) {
        // get compact SVD (D matrix is min(m,n) square)
        final SimpleSVD svd = x.svd(true);

        // preWhitening should only keep `numberOfComponents` columns if performing dimensionality reduction
        preWhitening = svd.getV().mult(svd.getW().invert()).extractMatrix(0, x.numCols(), 0, numberOfComponents);
        // preWhitening = preWhitening.scale(-1);
        // sklearn returns this version for preWhitening; doesn't affect results

        // return x.mult(preWhitening).scale(Math.sqrt(m));  // sklearn scales the input
        return x.mult(preWhitening);
    }

    /**
     * Center the input matrix and store it in dataMatrix by subtracting the average of
     * each column vector from every element in the column
     */
    private double[] center(final SimpleMatrix x) {
        final int rows = x.numRows();
        final int cols = x.numCols();
        final double[] means = new double[cols];
        for (int i = 0; i < cols; i++) {
            final SimpleMatrix col = x.extractVector(false, i);
            means[i] = col.elementSum() / (double) rows;
            for (int j = 0; j < rows; j++) {
                col.set(j, col.get(j) - means[i]);
            }
            dataMatrix.insertIntoThis(0, i, col);
        }
        return means;
    }

    /**
     * Perform symmetric decorrelation on the input matrix to ensure that each
     * column is independent from all the others. This is required in order
     * to prevent FastICA from solving for the same components in multiple
     * columns.
     *
     * NOTE: There are only real eigenvalues for the unMixingMatrix matrix
     *
     * unMixingMatrix <- unMixingMatrix * (unMixingMatrix.T * unMixingMatrix)^{-1/2}
     *
     * Python (Numpy):
     *   s, u = linalg.eigh(np.dot(unMixingMatrix.T, unMixingMatrix))
     *   unMixingMatrix = np.dot(unMixingMatrix, np.dot(u * (1. / np.sqrt(s)), u))
     * Matlab:
     *   B = B * real(inv(B' * B)^(1/2))
     */
    private static SimpleMatrix symmetricDeCorrelation(final SimpleMatrix x) {
        final SimpleEVD evd = x.transpose().mult(x).eig();
        final int len = evd.getNumberOfEigenvalues();
        final SimpleMatrix Q = new SimpleMatrix(len, len);
        final SimpleMatrix QL = new SimpleMatrix(len, len);

        // Scale each column of the eigenvector matrix by the square root of the reciprocal of the associated eigenvalue
        for (int i = 0; i < len; i++) {
            double d = evd.getEigenvalue(i).getReal();
            d = (d + Math.abs(d)) / 2;  // improve numerical stability by eliminating small negatives near singular matrix zeros
            QL.insertIntoThis(0, i, evd.getEigenVector(i).divide(Math.sqrt(d)));
            Q.insertIntoThis(0, i, evd.getEigenVector(i));
        }
        return x.mult(QL.mult(Q.transpose()));
    }

    /**
     * Randomly generate a square matrix drawn from a standard gaussian distribution.
     */
    private static SimpleMatrix gaussianSquareMatrix(final int size) {
        SimpleMatrix ret = new SimpleMatrix(size, size);
        Random rand = new Random();
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                ret.set(i, j, rand.nextGaussian());
            }
        }
        return ret;
    }

    /**
     * Convert a {@link SimpleMatrix} to a 2d array of double[][]
     */
    private static double[][] mToA(final SimpleMatrix x) {
        double[][] result = new double[x.numRows()][];
        for (int i = 0; i < x.numRows(); i++) {
            result[i] = new double[x.numCols()];
            for (int j = 0; j < x.numCols(); j++) {
                result[i][j] = x.get(i, j);
            }
        }
        return result;
    }

    /**
     * Return the matrix that projects the data into the ICA domain
     *
     * @return unMixingMatrix    double[][] matrix containing estimated independent component
     * projection matrix
     */
    public double[][] getUnMixingMatrix() {
        return mToA(unMixingMatrix);
    }

    /**
     * Return the pre-whitening matrix that was used on the data (defaults to
     * the identity matrix)
     *
     * @return preWhitening double[][] matrix containing the pre-whitening matrix
     */
    public double[][] getPreWhitening() {
        return mToA(preWhitening);
    }

    /**
     * Return the estimated mixing matrix that maps sources to the data domain
     * S * em = dataMatrix
     * @return em double[][] matrix containing the estimated mixing matrix
     */
    public double[][] getEM() {
        return mToA(preWhitening.mult(unMixingMatrix).pseudoInverse());
    }
}