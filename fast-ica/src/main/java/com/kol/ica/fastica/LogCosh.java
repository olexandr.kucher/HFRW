package com.kol.ica.fastica;

import org.ejml.simple.SimpleMatrix;

/**
 * Default function used in FastICA to approximate neg-entropy.
 */
public class LogCosh implements NegativeEntropyEstimator {

    // Element-wise application of the neg-entropy function applied to data matrix
    private SimpleMatrix elementWise;

    // Column-wise average of the first derivative of elementWise; i.e., the average of 1 - elementWise[i]**2
    private SimpleMatrix columnWise;

    // Scaling factor
    private final double alpha;

    public LogCosh(double alpha) {
        this.alpha = alpha;
    }

    public LogCosh() {
        this(1.0);
    }

    /**
     * @param x - {@link SimpleMatrix} of column vectors for each feature
     */
    @Override
    public void estimate(SimpleMatrix x) {
        final int rows = x.numRows();
        final int cols = x.numCols();
        elementWise = new SimpleMatrix(rows, cols);
        columnWise = new SimpleMatrix(1, cols);
        for (int j = 0; j < cols; j++) {
            double tmp = 0;
            for (int i = 0; i < rows; i++) {
                final double val = Math.tanh(x.get(i, j));
                elementWise.set(i, j, val);
                tmp += alpha * (1 - Math.pow(val, 2));
            }
            columnWise.set(0, j, tmp / (double) rows);
        }
    }

    @Override
    public SimpleMatrix getElementWise() {
        return elementWise;
    }

    @Override
    public SimpleMatrix getColumnWise() {
        return columnWise;
    }
}