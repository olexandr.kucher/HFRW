package com.kol.analysis.recognizer;

import Jama.Matrix;
import com.google.common.collect.Multimap;
import com.kol.analysis.KNN;
import com.kol.analysis.bean.AnalysisSettings;
import com.kol.analysis.bean.Mix;
import com.kol.analysis.bean.ProjectedTrainingMatrix;
import com.kol.recognition.common.Algorithm;
import com.kol.recognition.common.Image;
import com.kol.recognition.common.ImageUtils;
import scala.Tuple2;

import java.awt.image.BufferedImage;
import java.util.Arrays;
import java.util.List;

public abstract class Recognizer implements Algorithm {

    protected Multimap<String, Matrix> data;
    protected Multimap<String, Image> training;

    protected Matrix meanMatrix;
    protected int numberOfComponents;

    protected Matrix w;
    protected List<ProjectedTrainingMatrix> projectedTrainingSet;

    protected final int imageAsVectorLength;

    protected AnalysisSettings settings;

    protected Recognizer(Multimap<String, Matrix> data, int components, int vecLength, Multimap<String, Image> training, AnalysisSettings settings){
        if (components >= data.size()) {
            throw new RuntimeException("the expected dimensions could not be achieved!");
        }
        this.data = data;
        this.training = training;
        this.settings = settings;
        this.numberOfComponents = components;
        this.imageAsVectorLength = vecLength;

        init();
    }

    /**
     * The matrix has already been vectorized
     */
    protected Matrix countMean(final List<Matrix> input) {
        final int rows = input.get(0).getRowDimension();
        final int length = input.size();
        final Matrix all = new Matrix(rows, 1);
        input.forEach(all::plusEquals);
        return all.times(1.0 / length);
    }

    protected int[] getIndexesOfKEigenvalues(final double[] d, final int k) {
        final Mix[] mixes = new Mix[d.length];
        for (int i = 0; i < d.length; i++) {
            mixes[i] = new Mix(i, d[i]);
        }
        Arrays.sort(mixes);
        final int[] result = new int[k];
        for (int i = 0; i < k; i++) {
            result[i] = mixes[i].getIndex();
        }
        return result;
    }

    /**
     * extract features, namely w
     */
    protected abstract void init();

    public Matrix getMeanMatrix() {
        return meanMatrix;
    }

    public List<ProjectedTrainingMatrix> getProjectedTrainingSet() {
        return projectedTrainingSet;
    }

    public Matrix getW() {
        return w;
    }

    public Multimap<String, Image> getTraining() {
        return training;
    }

    protected Matrix transform(final Matrix vector) {
        return getW().transpose().times(vector.minus(getMeanMatrix()));
    }

    @Override
    public Tuple2<Boolean, String> classify(final BufferedImage image) {
        final Matrix testCase = transform(ImageUtils.toVector(ImageUtils.toMatrix(image)));
        final ProjectedTrainingMatrix[] trainingSet = projectedTrainingSet.toArray(new ProjectedTrainingMatrix[projectedTrainingSet.size()]);
        final Tuple2<String, Double> classified = KNN.assignLabel(trainingSet, testCase, settings.getKnnCount(), settings.getMetric());
        return null != classified._1() && classified._2() <= settings.getThreshold() ? new Tuple2<>(true, classified._1()) : new Tuple2<>(false, classified._1());
    }

    @Override
    public List<BufferedImage> getProcessedImages() {
        return ImageUtils.convertMatricesToImage(getW(), settings.getHeight(), settings.getWidth());
    }

    @SuppressWarnings("unused")
    public BufferedImage reconstruct(final int whichImage, final int dimensions) {
        if(dimensions > numberOfComponents) {
            throw new RuntimeException("dimensions should be smaller than the number of components");
        }
        final Matrix afterPCA = projectedTrainingSet.get(whichImage).getMatrix().getMatrix(0, dimensions-1, 0, 0);
        final Matrix eigen = w.getMatrix(0, imageAsVectorLength-1, 0, dimensions - 1);
        return ImageUtils.convertVectorToImage(eigen.times(afterPCA).plus(meanMatrix), settings.getHeight(), settings.getWidth());
    }
}