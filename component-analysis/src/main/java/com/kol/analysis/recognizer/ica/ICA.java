package com.kol.analysis.recognizer.ica;

import Jama.Matrix;
import com.google.common.collect.Multimap;
import com.kol.analysis.bean.AnalysisSettings;
import com.kol.analysis.bean.ProjectedTrainingMatrix;
import com.kol.analysis.recognizer.Recognizer;
import com.kol.ica.fastica.FastICA;
import com.kol.recognition.common.Image;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public final class ICA extends Recognizer {

    private final com.kol.ica.fastica.FastICA fastICA;

    public ICA(Multimap<String, Matrix> data, int components, int vecLength, Multimap<String, Image> training, AnalysisSettings settings) {
        super(data, components, vecLength, training, settings);
        fastICA = new FastICA();
        fastICA.fit(toMatrixColumnImages(data.values()).transpose().getArray(), numberOfComponents);

        this.projectedTrainingSet = data.entries().stream()
                .map(d -> new ProjectedTrainingMatrix(d.getKey(), transform(d.getValue())))
                .collect(Collectors.toList());

        this.w = new Matrix(fastICA.getEM()).transpose();
    }

    @Override
    protected void init() {}

    @Override
    protected Matrix transform(final Matrix vector) {
        return new Matrix(fastICA.transform(vector.transpose().getArray())).transpose();
    }

    private Matrix toMatrixColumnImages(final Collection<Matrix> list) {
        final List<Matrix> input = new ArrayList<>(list);
        final int row = input.get(0).getRowDimension();
        final int column = input.size();
        final Matrix x = new Matrix(row, column);

        for (int i = 0; i < column; i++) {
            x.setMatrix(0, row - 1, i, i, input.get(i));
        }
        return x;
    }
}