package com.kol.analysis;

import Jama.Matrix;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.kol.analysis.bean.ProjectedTrainingMatrix;
import com.kol.metric.Metric;
import scala.Tuple2;

import java.util.Arrays;
import java.util.Optional;

public final class KNN {

    public static Tuple2<String, Double> assignLabel(final ProjectedTrainingMatrix[] trainingSet, final Matrix testFace, final int k, final Metric<Matrix> metric) {
        return classify(findKNN(trainingSet, testFace, k, metric));
    }

    /**
     * testFace has been projected to the subspace
     */
    public static ProjectedTrainingMatrix[] findKNN(final ProjectedTrainingMatrix[] trainingSet, final Matrix testFace, final int k, final Metric<Matrix> metric) {
        final int numOfTrainingSet = trainingSet.length;
        assert k <= numOfTrainingSet : "k is lager than the length of trainingSet!";

        // initialization
        final ProjectedTrainingMatrix[] neighbors = new ProjectedTrainingMatrix[k];
        for (int i = 0; i < k; i++) {
            trainingSet[i].setDistance(metric.getDistance(trainingSet[i].getMatrix(), testFace));
//            System.out.println("index: " + i + " distance: " + trainingSet[i].getDistance());
            neighbors[i] = trainingSet[i];
        }

        // go through the remaining records in the trainingSet to find k nearest neighbors
        for (int i = k; i < numOfTrainingSet; i++) {
            trainingSet[i].setDistance(metric.getDistance(trainingSet[i].getMatrix(), testFace));
//            System.out.println("index: " + i + " distance: " + trainingSet[i].getDistance());
            int maxIndex = 0;
            for (int j = 0; j < k; j++) {
                if (neighbors[j].getDistance() > neighbors[maxIndex].getDistance()) {
                    maxIndex = j;
                }
            }
            if (neighbors[maxIndex].getDistance() > trainingSet[i].getDistance()) {
                neighbors[maxIndex] = trainingSet[i];
            }
        }
        return neighbors;
    }

    /**
     * get the class label by using neighbors
     */
    private static Tuple2<String, Double> classify(final ProjectedTrainingMatrix[] neighbors) {
        final Multimap<String, ProjectedTrainingMatrix> map = HashMultimap.create();
        for (ProjectedTrainingMatrix neighbor : neighbors) {
            map.put(neighbor.getLabel(), neighbor);
        }
        String mkey = null;
        int size = 0;
        for (String key : map.keySet()) {
            if(map.get(key).size() > size) {
                mkey = key;
                size = map.get(key).size();
            }
        }
        final String label = mkey;
        final Optional<ProjectedTrainingMatrix> minOpt = Arrays.stream(neighbors)
                .filter(ptm -> ptm.getLabel().equalsIgnoreCase(label))
                .min((o1, o2) -> Double.compare(o1.getDistance(), o2.getDistance()));
        if(minOpt.isPresent()) {
            final ProjectedTrainingMatrix min = minOpt.get();
            return new Tuple2<>(min.getLabel(), min.getDistance() / 1e6);
        } else {
            return new Tuple2<>(null, Double.MAX_VALUE);
        }
    }
}