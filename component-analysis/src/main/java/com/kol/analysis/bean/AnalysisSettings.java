package com.kol.analysis.bean;

import Jama.Matrix;
import com.kol.metric.Metric;

public final class AnalysisSettings {

    private int knnCount;
    private int width;
    private int height;
    private Metric<Matrix> metric;
    private double threshold;

    public AnalysisSettings() {}

    public int getKnnCount() {
        return knnCount;
    }

    public void setKnnCount(int knnCount) {
        this.knnCount = knnCount;
    }

    public Metric<Matrix> getMetric() {
        return metric;
    }

    public void setMetric(Metric<Matrix> metric) {
        this.metric = metric;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public double getThreshold() {
        return threshold;
    }

    public void setThreshold(double threshold) {
        this.threshold = threshold;
    }
}
