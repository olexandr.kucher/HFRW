package com.kol.perceptualhash.monochrome;

import java.awt.image.BufferedImage;

public interface ToMonochrome {

    BufferedImage apply(BufferedImage src);
}
