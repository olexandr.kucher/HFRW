package com.kol.perceptualhash.hash;

import com.google.common.collect.Multimap;
import com.kol.metric.Metric;
import com.kol.perceptualhash.Hash;
import com.kol.recognition.common.Algorithm;
import com.kol.recognition.common.Image;
import scala.Tuple2;

import java.awt.image.BufferedImage;
import java.util.List;
import java.util.stream.Collectors;

public class HashRecognizer implements Algorithm {

    private PerceptualHash hash;
    private Metric<String> distance;
    private Multimap<String, Hash> data;
    private Multimap<String, Image> train;

    private double threshold;

    public HashRecognizer(PerceptualHash hash, Metric<String> distance, Multimap<String, Hash> data, Multimap<String, Image> train, double threshold) {
        this.hash = hash;
        this.distance = distance;
        this.data = data;
        this.train = train;
        this.threshold = threshold;
    }

    @Override
    public Tuple2<Boolean, String> classify(BufferedImage image) {
        final Hash imageHash = hash.getHash(image);
        double resultDistance = Double.MAX_VALUE;
        String resultClassName = null;
        for (String className : data.keySet()) {
            for (Hash hash : data.get(className)) {
                final double distance = this.distance.getDistance(hash.getHash(), imageHash.getHash());
                if(distance < resultDistance) {
                    resultDistance = distance;
                    resultClassName = className;
                }
            }
        }
        return resultDistance <= threshold ? new Tuple2<>(true, resultClassName) : new Tuple2<>(false, resultClassName);
    }

    @Override
    public Multimap<String, Image> getTraining() {
        return train;
    }

    @Override
    public List<BufferedImage> getProcessedImages() {
        return data.values().stream().map(Hash::getImage).collect(Collectors.toList());
    }
}