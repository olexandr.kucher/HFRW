package com.kol.perceptualhash.hash;

import com.google.common.collect.Lists;
import com.kol.perceptualhash.Hash;
import com.kol.perceptualhash.bitsChain.BitsChainBigIntToString;
import com.kol.perceptualhash.bitsChain.BitsChainToString;
import com.kol.perceptualhash.monochrome.ToByteGray;
import com.kol.perceptualhash.monochrome.ToMonochrome;
import com.kol.perceptualhash.resize.ResizeImage;
import com.kol.perceptualhash.resize.ScalrResize;
import com.kol.recognition.common.RGBImage;

import java.awt.image.BufferedImage;
import java.util.List;

public final class AverageHash extends PerceptualHash {

    public AverageHash(int smallWidth, int smallHeight,
                       ResizeImage resizeImage,
                       ToMonochrome toMonochrome,
                       BitsChainToString toString) {
        super(smallWidth, smallHeight, resizeImage, toMonochrome, toString);
    }

    public AverageHash(int smallWidth, int smallHeight) {
        super(smallWidth, smallHeight, new ScalrResize(), new ToByteGray(), new BitsChainBigIntToString());
    }

    @Override
    public Hash getHash(BufferedImage image) {
        final BufferedImage resize = resize(image, width, height);
        final BufferedImage monochrome = toMonochrome(resize);
        final RGBImage rgbImage = RGBImage.fromBufferedImage(monochrome);
        final double mean = meanPixelValue(rgbImage);
        final String bitsChain = toBitsChain(rgbImage, mean);
        return new Hash(bitsChainToString(bitsChain), monochrome);
    }

    @Override
    public List<BufferedImage> getHashImage(BufferedImage image) {
        final BufferedImage resize = resize(image, width, height);
        final BufferedImage monochrome = toMonochrome(resize);
        final RGBImage rgbImage = RGBImage.fromBufferedImage(monochrome);
        final double mean = meanPixelValue(rgbImage);
        return Lists.newArrayList(toBitsChainImgContent(rgbImage, mean), monochrome);
    }
}