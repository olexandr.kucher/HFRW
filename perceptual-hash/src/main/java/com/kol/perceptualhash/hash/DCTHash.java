package com.kol.perceptualhash.hash;

import com.google.common.collect.Lists;
import com.kol.dct.DCT;
import com.kol.perceptualhash.Hash;
import com.kol.perceptualhash.bitsChain.BitsChainBigIntToString;
import com.kol.perceptualhash.bitsChain.BitsChainToString;
import com.kol.perceptualhash.monochrome.ToByteGray;
import com.kol.perceptualhash.monochrome.ToMonochrome;
import com.kol.perceptualhash.resize.ResizeImage;
import com.kol.perceptualhash.resize.ScalrResize;
import com.kol.recognition.common.RGBImage;
import com.kol.recognition.common.Utils;

import java.awt.image.BufferedImage;
import java.util.List;

public final class DCTHash extends PerceptualHash {

    public DCTHash(int width, int height,
                   ResizeImage resizeImage,
                   ToMonochrome toMonochrome,
                   BitsChainToString bitsChainToString) {
        super(width, height, resizeImage, toMonochrome, bitsChainToString);
    }

    public DCTHash(int smallWidth, int smallHeight) {
        super(smallWidth, smallHeight, new ScalrResize(), new ToByteGray(), new BitsChainBigIntToString());
    }

    @Override
    public Hash getHash(final BufferedImage image) {
        final BufferedImage resize = resize(image, width, height);
        final BufferedImage monochrome = toMonochrome(resize);
        final RGBImage rgbImage = RGBImage.fromBufferedImage(monochrome);
        final double mean = Utils.mean(DCT.dct(Utils.toDouble(rgbImage.content())));
        final String bitsChain = toBitsChain(rgbImage, mean);
        return new Hash(bitsChainToString(bitsChain), monochrome);
    }

    @Override
    public List<BufferedImage> getHashImage(final BufferedImage image) {
        final BufferedImage resize = resize(image, width, height);
        final BufferedImage monochrome = toMonochrome(resize);
        final RGBImage rgbImage = RGBImage.fromBufferedImage(monochrome);
        final double mean = Utils.mean(DCT.dct(Utils.toDouble(rgbImage.content())));
        return Lists.newArrayList(toBitsChainImgContent(rgbImage, mean), monochrome);
    }
}