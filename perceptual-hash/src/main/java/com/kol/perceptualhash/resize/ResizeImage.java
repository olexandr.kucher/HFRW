package com.kol.perceptualhash.resize;

import java.awt.image.BufferedImage;

public interface ResizeImage {
    BufferedImage resize(BufferedImage src, int width, int height);
}
